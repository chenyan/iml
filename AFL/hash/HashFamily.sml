(********************************************************************** 
 ** HashFamily.sml
 **
 ** Copyright 2005 Umut A. Acar
 **
 ** Written by Umut A. Acar.
 **
 ** This program is distributed under the terms of the GNU General
 ** Public License as published by the Free Software Foundation.  See
 ** the LICENSE file for the terms and conditions.
 **
 ***************
 **
 ** Description: Provides a family of random hash functions.
 ** No independence-guarantees provided.
 **********************************************************************)

functor HashFamily (val M: int) : HASH_FAMILY = struct
  open Random

  (* Some constants *)
  val SOME(maxInt) = Int.maxInt
  val SOME(minInt) = Int.minInt

  (* A large prime *)
  val P = 1073741789
  val realP = Real.fromInt P

  (* Internal state for SML random number generator *)
  val state  = Random.rand (minInt,maxInt)

  fun hashFun () = 
    let
      val a = Real.fromInt (Random.randInt state)
      val b = Real.fromInt (Random.randInt state)

      fun hash x = 
	let
          val realx = Real.fromInt x
	  val s = a*realx + b
	  val smodp = s - (Real.realFloor(s/realP) * realP)
	in
          (Real.trunc smodp)  mod M
	end
    in
      hash
    end

end





