(**********************************************************************
 ** COMBINATORS.sml
 **
 ** Copyright 2005 Umut A. Acar
 **
 ** Written by Umut A. Acar and Matthias Blume
 **
 ** This program is distributed under the terms of the GNU General
 ** Public License as published by the Free Software Foundation.  See
 ** the LICENSE file for the terms and conditions.
 **
 ***************
 ** Description: A combinator library for modifiables.
 **********************************************************************)
signature COMBINATORS =
sig   
  type 'a modref   = 'a Modref.t 
  type 'a cc
  type changeable = Modref.changeable

  val modref : 'a cc -> 'a modref
  val fromVal : 'a -> 'a modref
  val new: unit -> 'a modref

  (* Pure read/write *)
  val write : 'a Box.t -> 'a Box.t cc
  val write' : ('a * 'a -> bool) -> 'a -> 'a cc
  val write_eq : ''a -> ''a cc
  val write_force: 'a -> 'a cc 
  val read : 'b Modref.t * ('b -> 'a cc) -> 'a cc
  val read_notime : 'b Modref.t * ('b -> 'a cc) -> 'a cc

  (* Impure read/write *)
  val iwrite : 'a Box.t modref -> 'a Box.t -> changeable
  val iwrite' : ('a * 'a -> bool) -> 'a modref -> 'a -> changeable
  val iread : 'a modref  -> ('a -> changeable) -> changeable

  val icopy : 'a Box.t modref -> 'a Box.t modref -> unit


  val mkLiftPure : unit -> (int list) -> (unit -> 'a) -> 'a

  val mkLiftOptimized: ('a Box.t -> 'b) -> (('a Box.t -> ''c -> 'd) -> 'b -> ''c -> 'd) ->
		       ('a Box.t -> ''c -> 'd)

  val mkLiftOptimized': ('c * 'c -> bool) ->  ('a Box.t -> 'b) -> (('a Box.t -> 'c -> 'd) -> 'b -> 'c -> 'd) ->
		        ('a Box.t -> 'c -> 'd)

  val mkLift0 : unit -> (int list) -> (unit -> 'a) -> 'a

  val mkLift : ('b *'b -> bool) -> (int list * 'b) -> 
               ('b Modref.t -> 'd) ->  'd


  val mkLift1 : ('b *'b -> bool) -> (int list * 'b) -> 
               ('b Modref.t -> 'd) ->  'd

  val mkLift2 : (('b * 'b -> bool) * ('c * 'c -> bool)) -> 
                int list * 'b * 'c -> ('b Modref.t * 'c Modref.t -> 'd) -> 'd

  val mkLift3 : (('b * 'b -> bool) * ('c * 'c -> bool) * ('d * 'd -> bool)) -> 
                int list * 'b * 'c * 'd -> 
		('b Modref.t * 'c Modref.t * 'd Modref.t-> 'e) -> 'e

  val mkLift4 : (('b * 'b -> bool) * ('c * 'c -> bool) * ('d * 'd -> bool) * ('e * 'e -> bool)) -> 
                int list * 'b * 'c * 'd * 'e -> 
		('b Modref.t * 'c Modref.t * 'd Modref.t * 'e Modref.t -> 'f) -> 'f


  val mkLiftCC0 : ('b * 'b -> bool) -> int list -> (unit -> 'b cc) -> 'b cc

  val mkLiftCC : (('b * 'b -> bool) * ('d * 'd -> bool))  -> 
                 int list * 'b -> ('b Modref.t -> 'd cc) -> 'd cc

  val mkLiftCC1 : (('b * 'b -> bool) * ('d * 'd -> bool))  -> 
                 int list * 'b -> ('b Modref.t -> 'd cc) -> 'd cc

  val mkLiftCC2 : (('b * 'b -> bool) * ('c *'c -> bool) * ('d * 'd -> bool)) -> 
                  int list * 'b * 'c -> ('b modref * 'c modref -> 'd cc) -> 'd cc

  val mkLiftCC3 : (('a * 'a -> bool)*('b * 'b -> bool) * ('c *'c -> bool) * ('d * 'd -> bool)) -> 
                  int list *'a* 'b * 'c -> ('a modref * 'b modref * 'c modref -> 'd cc) -> 'd cc

  val mkLiftCC4 : (('a * 'a -> bool)*('b * 'b -> bool) * ('c *'c -> bool) * ('d * 'd -> bool)*('e * 'e -> bool)) -> 
                  int list * 'a * 'b * 'c * 'd -> ('a modref * 'b modref * 'c modref * 'd modref  -> 'e cc) -> 'e cc

  val mkLiftCC5 : (('a * 'a -> bool)*('b * 'b -> bool) * ('c *'c -> bool) * ('d * 'd -> bool)*('e * 'e -> bool)*('f * 'f -> bool)) -> 
                  int list * 'a * 'b * 'c * 'd * 'e -> ('a modref * 'b modref * 'c modref * 'd modref * 'e modref  -> 'f cc) -> 'f cc



  val mkLift0_one :  unit ->  int list -> (unit -> 'd) ->  'd

  val mkLift1_one : ('b *'b -> bool) -> (int list * 'b) -> 
               ('b Modref.t -> 'd) ->  'd

  val mkLift_one : ('b *'b -> bool) -> (int list * 'b) -> 
               ('b Modref.t -> 'd) ->  'd

  val mkLift2_one : (('b * 'b -> bool) * ('c * 'c -> bool)) -> 
                int list * 'b * 'c -> ('b Modref.t * 'c Modref.t -> 'd) -> 'd

  val mkLift3_one : (('b * 'b -> bool) * ('c * 'c -> bool) * ('d * 'd -> bool)) -> 
                int list * 'b * 'c * 'd -> 
		('b Modref.t * 'c Modref.t * 'd Modref.t-> 'e) -> 'e

  val mkLift4_one : (('b * 'b -> bool) * ('c * 'c -> bool) * ('d * 'd -> bool) * ('e * 'e -> bool)) -> 
                int list * 'b * 'c * 'd * 'e -> 
		('b Modref.t * 'c Modref.t * 'd Modref.t * 'e Modref.t -> 'f) -> 'f


  val mkLiftCC0_one : ('b * 'b -> bool) -> int list -> (unit -> 'b cc) -> 'b cc

  val mkLiftCC_one : (('b * 'b -> bool) * ('d * 'd -> bool))  -> 
                 int list * 'b -> ('b Modref.t -> 'd cc) -> 'd cc

  val mkLiftCC1_one : (('b * 'b -> bool) * ('d * 'd -> bool))  -> 
                 int list * 'b -> ('b Modref.t -> 'd cc) -> 'd cc

  val mkLiftCC2_one : (('b * 'b -> bool) * ('c *'c -> bool) * ('d * 'd -> bool)) -> 
                  int list * 'b * 'c -> ('b modref * 'c modref -> 'd cc) -> 'd cc

  val mkLiftCC3_one : (('a * 'a -> bool)*('b * 'b -> bool) * ('c *'c -> bool) * ('d * 'd -> bool)) -> 
                  int list *'a* 'b * 'c -> ('a modref * 'b modref * 'c modref -> 'd cc) -> 'd cc

  val mkLiftCC4_one : (('a * 'a -> bool)*('b * 'b -> bool) * ('c *'c -> bool) * ('d * 'd -> bool)*('e * 'e -> bool)) -> 
                  int list * 'a * 'b * 'c * 'd -> ('a modref * 'b modref * 'c modref * 'd modref  -> 'e cc) -> 'e cc

  val mkLiftCC5_one : (('a * 'a -> bool)*('b * 'b -> bool) * ('c *'c -> bool) * ('d * 'd -> bool)*('e * 'e -> bool)*('f * 'f -> bool)) -> 
                  int list * 'a * 'b * 'c * 'd * 'e -> ('a modref * 'b modref * 'c modref * 'd modref * 'e modref  -> 'f cc) -> 'f cc

end
