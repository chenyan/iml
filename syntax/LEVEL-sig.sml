signature LEVEL =
sig
   datatype t = Stable
              | Changeable
              | LVar of LvVar.LvVar
              | Unknown
              | Undetermined of LExp.lexp

   val toString : t -> string

end
