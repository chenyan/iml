(**********************************************************************
 ** Modref.sml
 **
 ** Copyright 2005 Umut A. Acar
 **
 ** Written by Umut A. Acar, Guy Blelloch, Robert Harper
 **
 ** This program is distributed under the terms of the GNU General
 ** Public License as published by the Free Software Foundation.  See
 ** the LICENSE file for the terms and conditions.
 **
 ***************
 ** Description: This implementation is described in Acar's thesis
 ** "Self-Adjusting Computation", (Carnegie Mellon University
 ** Department of Computer Science, Technical Report. CMU-CS-05-129).
 **********************************************************************)

structure Modref : MODIFIABLE=
struct 

type time = TimeStamps.t
type changeable = unit 
 

exception UnsetMod 
exception AssertionFail
exception DummyRead
exception BadCode
exception InternalError
exception UnsafeUseOfReadNoTime

(**********************************************************************
 ** Time Stamps
 **********************************************************************)
val base = TimeStamps.init ()
val now = ref (TimeStamps.add (base))
val frameStart = ref (!now)
val finger = ref (!now)

fun insertTime () = 
  let val t = TimeStamps.add (!now) 
  in 
    now := t; t
  end 

fun setTime t = now := t

(**********************************************************************
 ** Statistics
 **********************************************************************)
val N_NO_READS = ref 0

fun inc r = r := (!r) + 1
fun reset r = r := 0
  
(**********************************************************************
 ** Priority Queue
 **********************************************************************)
structure Closure = 
struct
type t = ((unit -> unit) * time * time) 
fun compare (a as (ca,sa,ea), b as (cb,sb,eb)) = (TimeStamps.compare(sa,sb))
fun isValid (c,s,e) = not (TimeStamps.isSplicedOut s)
end

structure PQueue = LeftistHeap (structure Element=Closure)

type pq = PQueue.t
val PQ = ref PQueue.empty
fun initQ() = (PQ := PQueue.empty)
	      
fun insertQ e =  PQ := PQueue.insert (e,!PQ) 

fun findMinQ () = 
    let val (m,q) = PQueue.findMin (!PQ) 
      val _ = PQ := q
    in m end

fun deleteMinQ () = 
    let val (m,q) = PQueue.deleteMin (!PQ) 
      val _ = PQ := q
    in m end

fun resetQ () = PQ := PQueue.empty
fun checkQ () = print ("queue size = " ^ Int.toString (PQueue.size  (!PQ)) ^ "\n")

fun findMinQ () = 
  let 
    val (m,q) = PQueue.findMin (!PQ) 
    val _ = PQ := q
  in
    case m  of
      NONE => NONE
    | SOME (_,t,_) => 
      if (TimeStamps.isSplicedOut t) then
	(deleteMinQ ();  findMinQ ())
      else
	m
  end

fun init () = (now := TimeStamps.init(); reset N_NO_READS; initQ())

(**********************************************************************
 ** Modifiables
 **********************************************************************)
type reader = (unit -> unit) * time * time 

datatype  readers = ZERO 
		  | ONE of reader 
		  | FUN of ((readers ref) *  reader * (readers ref))

datatype 'a modval = EMPTY | WRITE of 'a * readers
				      
type 'a modref = int * ('a modval ref)
type 'a t = 'a modref


val id = ref 0 
fun nextid () = (id := (!id) + 1; !id)

val dummyDelete: unit -> unit = fn () => ()
					 
fun hasNoReads ((_,r): 'a modref) = 
  case !r of
    EMPTY => true
  | WRITE (_,ZERO) => true
  | _ => (inc N_NO_READS; false)

fun empty () =  (nextid (), ref EMPTY)

fun new v = (nextid (), ref (WRITE (v,ZERO)))

fun modref f =
  let 
    val r = (nextid (), ref EMPTY)
  in 
    (f r; r) 
  end

fun read (_,modr) f = 
  let
    fun delete node = 
     fn () => (
       case node of 
	  ONE _ =>
	    let
	      val WRITE (v, rs) = !modr
	    in
	      case rs of
		ONE _ => modr := WRITE(v,ZERO) 
	      | FUN (p, reader as (_,t,_), n as ref next) => 
		let
		  val new = ONE reader
		  val _ = TimeStamps.setInv (t,delete new)
		in
		  case next of 
		    ZERO => modr := WRITE (v, new)
		  | FUN(pofn,_,_) => (pofn := new; modr := WRITE (v, next))
		  | _ => raise InternalError
		end
	    end

	| FUN (p as ref prev, _, n as ref next ) =>  
	  case (prev,next) of
	    (ONE _, ZERO) => 
	    let val WRITE (v,_) = !modr in modr := WRITE (v,prev) end 
	  | (FUN (_,_, nofp), ZERO) => nofp := ZERO
	  | (ONE _, FUN (pofn, _,_)) => 
	    let val WRITE(v,_) = !modr val _ = pofn := prev in  modr := WRITE (v,next) end
	  | (FUN(_, _, nofp), FUN(pofn,_,_)) => (nofp := next; pofn := prev)
	  | _ => raise InternalError)

	 
    val WRITE (v,rs) = !modr

    fun run () = let val WRITE(v,_) = !modr in f v end
		 
    val t1 = insertTime ()
    val _ = f v
    val t2 = insertTime ()
    val WRITE(v,rs) = !modr
		      
    val new = case rs of
		ZERO => ONE (run, t1, t2)
	      | ONE _ => FUN (ref rs, (run, t1, t2), ref ZERO)
	      | FUN (prev,_,next) => 
		let val new = FUN (ref (!prev), (run, t1, t2), ref rs)
		  val _ = prev := new
		in new end
    val _ = TimeStamps.setInv (t1, delete new)  
  in
	   modr := WRITE (v,new)
  end

fun read_notime (_,modr) f = 
  case !modr of
    EMPTY => raise UnsetMod
  | WRITE (v,rs) =>  
    let
      val t1 = TimeStamps.special
      val _ = f v
      val t2 = TimeStamps.special

      fun run () = 
        case !modr of
	  EMPTY => raise UnsetMod
	| WRITE(v,_) => (modr := WRITE (v,ONE(run,t1,t2)); f v)
    in
      modr := WRITE (v,ONE(run,t1,t2))
    end


fun write' comp (_,modr) v =
  let
    fun processReader (r as (f,(t:TimeStamps.t),_)) = 
      if TimeStamps.isSpecial t then 
	f ()
      else
	(insertQ r)

    fun qReaders l = 
      case l of 
	ZERO => ()
      | FUN(p,r, ref next) => (processReader r ; qReaders next)
  in      	
    case !modr of
      EMPTY => modr := WRITE (v, ZERO)
    | WRITE(v',rs) =>
      if comp (v,v') then ()
      else let val _ = modr := WRITE(v,rs) 
	   in
	     case rs of 
	       ZERO => ()
	     | ONE r  => processReader r
	     | FUN(ref (ONE r),_,next) => (processReader r; qReaders rs)
	   end
  end
  
fun write modr v = write' Box.eq modr v

fun cancel (_,modr) =
  let
    val readList = ref []

    fun processReader (r as (f,t1,t2)) = 
      if TimeStamps.isSpecial t1 then  ()
      else readList := r::(!readList)
  
    fun processReaders rs = 
      case rs of
	ZERO => ()
      | FUN (_, r, ref next) => (processReaders next; processReader r)
				
    fun processReaderList l = 
        case l of 
	  nil => ()
	| (r as (_,t1,t2))::t => 
	  if (TimeStamps.isSplicedOut t1) then
	    if (TimeStamps.isSplicedOut t2) then
	      processReaderList t
	    else
	      raise InternalError
	  else
	    (TimeStamps.spliceOut (t1,t2); processReaderList t)
    val _ = 
	case !modr of
	  EMPTY => ()
	| WRITE(_,rs) =>
	  case rs of 
	    ZERO => ()
	  | ONE r  => processReader r
	  | FUN(ref (ONE r),_,next) => (processReaders rs; processReader r)
  in
    processReaderList (!readList)
  end

fun deref (_,modr) =
  case !modr of
    EMPTY => raise UnsetMod
  | WRITE (v,_) => v

fun idOf (id,_) = id

(**********************************************************************
 ** Change propagation
 **********************************************************************)
fun propagateUntil (endTime) =  
  let
    fun loop  () = 
      case  (findMinQ ()) of
	NONE => ()
      | SOME(f,start,stop) =>
	if (TimeStamps.compare (endTime,stop)=LESS) then ()
	else if (TimeStamps.compare (start, !now)= LESS) then ()
	else
	  let
            val _ = deleteMinQ ()
	    val finger' = (!finger)
            val _ = now := start;	   
            val _ = finger := stop;
            val _ = f();
            val _ = finger := finger';
	    val _ = TimeStamps.spliceOut (!now,stop) handle e => raise e; 
          in
            loop ()	
	  end
  in	    
    (loop (); now := endTime)
  end

fun propagate () =  
  let
    val _ = now := base
    fun loop  () = 
      case  (findMinQ ()) of
	NONE => 0
      | SOME(f,start,stop) =>
	if (TimeStamps.compare (start,!now)= LESS) then (0)
	else
	  let
            val _ = deleteMinQ ()
            val _ = now := start
	    val _ = finger := stop
            val _ = f()
	    val _ = TimeStamps.spliceOut (!now,stop) handle e => raise e
          in
            1+loop ()	
	  end
       val r = loop ()
    in	    
	r
    end

fun resetPropagation () = resetQ ()
			  
fun isOutOfFrame(start,stop) =
    not (TimeStamps.compare(!now,start) = LESS andalso 
        TimeStamps.compare(stop,!finger) = LESS)


fun change l v = write l v
fun change' comp l v = write' comp l v
fun change'' (l: 'a modref) v = write' (fn _ => false) l v

fun numTimeStamps () = TimeStamps.size( !now)

end 
