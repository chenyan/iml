(********************************************************************
 ** MemoTable.sml
 **
 ** Copyright 2005 Umut A. Acar
 **
 ** Written by Umut A. Acar and Guy Blelloch.
 **
 ** This program is distributed under the terms of the GNU General
 ** Public License as published by the Free Software Foundation.  See
 ** the LICENSE file for the terms and conditions.
 **
 ***************
 ** Description: An implementation of memo tables.
 **********************************************************************)

structure MemoTable: MEMO_TABLE = 
struct

val initSize = 4  (* Must be a power of 2 *)
val initMask = (Word.fromInt initSize - 0w1)
val GROWTH_RATIO = 4

type 'a entry = ('a * TimeStamps.t * TimeStamps.t) option ref
		  
exception InternalError

val MASK_EVEN = 0wx55555554  (* Even bits are one *)
val MASK_ODD = 0wx2aaaaaaa   (* Odd bits are one *)

fun growRatio (size) = 
  if size < 1024 then  8
  else 8

(**********************************************************************
 ** General Hash Tables
 **********************************************************************)
datatype 'a bucket = EMPTY | B of (int list * 'a * 'a bucket)

type 'a memotable = {buckets : 'a entry bucket array,
		     mask : word,
		     numItems : int} ref

type 'a t = 'a memotable

fun new () = ref {buckets = Array.array(initSize,EMPTY),
		  mask = initMask,
		  numItems = 0} 

fun hash mask l = 
  case l of 
    nil => raise InternalError
  | h::nil => 
    let
      val w = Word.fromInt h
      val i = Word.toIntX(Word.andb(w, mask))
    in
      i
    end

  | h1::h2::_ => 
    let
      val w1 = Word.fromInt h1
      val w2 = Word.fromInt h2
      val w = Word.andb (w1,MASK_EVEN) + Word.andb(w2,MASK_ODD)
      val i = Word.toIntX(Word.andb(w, mask))
    in
      i
    end

fun listEq (a,b) =   
  let
    fun toString l = List.foldr (fn (k,s) => (Int.toString k ^ "," ^ s)) "" l

    fun eq (a,b) =  
      case (a,b) of
	(nil,nil) => true
      | (ha::ta,hb::tb) => ha=hb andalso eq(ta,tb)
      | _ => false
  in eq (a,b) end

fun growTable (table : 'a memotable, newSize : int) : unit =
  let
    val {buckets, mask, numItems} = !table
    val nDeleted = ref 0
    val newArray = Array.array (newSize, EMPTY)
    val newMask = (Word.fromInt newSize - 0w1)

    fun lengthOf b = 
      case b of
        EMPTY => 0
      | B(_,_,rest) => 1 + lengthOf rest
		       
    fun copy b = 
      let
	fun c EMPTY = ()
	  | c (B(h, e, rest)) = 
	    let
	      val _ = c rest
	      val i = hash newMask h
	      val l = Array.sub (newArray, i) 
	      val _ = Array.update (newArray, i, B(h, e, l))
	    in
	      ()
	    end
      in
	c b
      end
    val _ = Array.app copy buckets
  in
    table := {buckets=newArray, mask=newMask, numItems=numItems-(!nDeleted)}
  end

fun delete (table: 'a memotable, key: int list) (ta:TimeStamps.t, tb: TimeStamps.t) = 
  let
    val {buckets,mask,numItems} = !table
    val i = hash mask key
    val l = Array.sub (buckets,i)

    val flag = ref false
    fun find l =
      case l of
	EMPTY => EMPTY
      | (B(h, v, rest)) =>
	if listEq (key,h) then
	  case !v of 
	    NONE => raise InternalError
	  | SOME(_,t1,t2) =>  
            if ta=t1 andalso tb=t2 then
              (flag := true; rest)
	    else (find rest)
	else 
          let val rest' = find rest
          in B(h,v,rest')
          end
    val l' = find l
  in
    if (!flag) then
      (
       Array.update (buckets,i,l'); 
       table := {buckets = buckets, mask=mask, numItems=numItems-1};
       ()
      )
    else
      ()
  end

fun find (table : 'a memotable, key : int list, currentTime: TimeStamps.t) : 'a entry = 
  let
    val {buckets,mask,numItems} = !table
    val i = hash mask key
    val l = Array.sub (buckets,i)
    val size = Array.length buckets

    fun updateTable entry l = 
      case (!entry) of 
	NONE => 
	let
	  val _ = Array.update(buckets, i, l)
	  val _ = table := {buckets=buckets, mask=mask, numItems=numItems+1}
	in
	  if (numItems >= 2 * size) then 
	    growTable(table, (growRatio size) * size)
	  else 
	    ()
	end
      | _ => ()

    fun look l =
      case l of
	EMPTY =>
	let val entry = ref NONE
        in (entry, B(key,entry,l))
        end
      | B(h, v, rest) =>
	if listEq (key,h) then
	  case !v of 
	    NONE => 
              let val (entry,rest') = look rest
              in (entry,B(h,v,rest'))
              end
	  | SOME(_,t1,t2) => 
            if (TimeStamps.isSplicedOut t1) then
              let val (entry,rest') = look rest
              in (entry,B(h,v,rest'))
              end
            else if Modref.isOutOfFrame(t1,t2) then
              let val (entry,rest') = look rest
              in (entry,B(h,v,rest'))
              end
	    else (v,l) 
	else 
	  let val (entry,rest') = look rest
	  in (entry,B(h,v,rest'))
	  end
    val (entry,l') = look l
    val () = updateTable entry l'
  in
    entry
  end

fun verify (table:'a memotable) : unit =
  let
    val {buckets, mask, numItems} = !table
    val n = ref 0

    fun length b = 
      case b of
        EMPTY => 0
      | B(_,_,rest) => 1 + length rest

    val n = Array.foldr (fn (b,n) => length b + n) 0 buckets
    val _ = if (n = numItems) then ()
	    else print ("Oops, n = " ^ Int.toString n ^ " numItems = " ^ Int.toString numItems ^ "\n")
  in
    ()
  end

fun numItems (table: 'a memotable) = 
  let
    val {buckets, mask, numItems} = !table
  in
    numItems
  end

(**********************************************************************
 ** One
 **********************************************************************)

datatype 'a bucket_one = EMPTY_ONE | B_ONE of (int * 'a * 'a bucket_one)

type 'a memotable_one = {buckets : 'a entry bucket_one array,
			 mask : word,
			 numItems : int} ref

type 'a t_one = 'a memotable_one

fun newOne () = ref {buckets = Array.array(initSize,EMPTY_ONE),
		     mask = initMask,
		     numItems = 0} 


fun hashOne mask key = Word.toIntX(Word.andb(Word.fromInt key, mask))


fun deleteOne (table: 'a memotable_one, key::nil) (ta:TimeStamps.t, tb: TimeStamps.t) = 
  let
    val {buckets,mask,numItems} = !table
    val i = hashOne mask key
    val l = Array.sub (buckets,i)

    val flag = ref false
    fun find l =
      case l of
	EMPTY_ONE => EMPTY_ONE
      | B_ONE(h, v, rest) =>
	if key=h then
	  case !v of 
	    NONE => raise InternalError
	  | SOME(_,t1,t2) =>  
            if (ta=t1 andalso tb=t2) then
              (flag := true; rest)
	    else (find rest)
	else 
          let val rest' = find rest
          in B_ONE(h,v,rest')
          end
    val l' = find l
  in
    if (!flag) then
      (
       Array.update (buckets,i,l'); 
       table := {buckets = buckets, mask=mask, numItems=numItems-1};
       ()
      )
    else
      ()
  end

fun growTableOne (table:'a memotable_one, newSize) : unit =
  let
    val {buckets, mask, numItems} = !table
    val nDeleted = ref 0
    val newArray = Array.array (newSize, EMPTY_ONE)
    val newMask = (Word.fromInt newSize - 0w1)

    fun lengthOf (b: 'a bucket_one) = 
      case b of
        EMPTY_ONE => 0
      | B_ONE(_,_,rest) => 1 + lengthOf rest
			   
    fun copy b = 
      let
	fun c EMPTY_ONE = ()
	  | c (B_ONE(h, e, rest)) = 
	    let
	      val _ = c rest
	      val i = hashOne newMask h
	      val l = Array.sub (newArray, i) 
	      val _ = Array.update (newArray, i, B_ONE(h, e, l))
	    in
	      ()
	    end
      in
	c b
      end

    val _ = Array.app copy buckets
  in
    table := {buckets=newArray, mask=newMask, numItems=numItems-(!nDeleted)}
  end

fun findOne (table: 'a memotable_one, key::nil, currentTime) : 'a entry =  
  let
    val {buckets,mask,numItems} = !table
    val i = hashOne mask key
    val l = Array.sub (buckets,i)
    val size = Array.length buckets

    fun updateTable entry l = 
      case (!entry) of 
	NONE => 
	let
	  val _ = Array.update(buckets, i, l)
	  val _ = table := {buckets=buckets, mask=mask, numItems=numItems+1}
	in
	  if (numItems >= 4 * size) then 
	    growTableOne(table,  (growRatio size) * size)
	  else 
	    ()
	end
      | _ => ()

    fun look l =
      case l of
	EMPTY_ONE =>
	let val entry = ref NONE
        in (entry, B_ONE(key,entry,l))
        end
      | (B_ONE(h, v, rest)) =>
	if key=h then
	  case !v of 
	    NONE => 
              let val (entry,rest') = look rest
              in (entry,B_ONE(h,v,rest'))
              end
	  | SOME(_,t1,t2) => 
            if (TimeStamps.isSplicedOut t1) then
              let val (entry,rest') = look rest
              in (entry,B_ONE(h,v,rest'))
              end
            else if Modref.isOutOfFrame(t1,t2) then 
              let val (entry,rest') = look rest
              in (entry,B_ONE(h,v,rest'))
              end
	    else (v,l) 
	else 
          let val (entry,rest') = look rest
          in (entry,B_ONE(h,v,rest'))
          end
    val (entry,l') = look l
    val () = updateTable entry l'
  in
    entry
  end

fun findOne (table: 'a memotable_one, keys, currentTime) : 'a entry =  
  case keys of
    nil => raise InternalError
  | key::_ => 
  let
    val {buckets,mask,numItems} = !table
    val i = hashOne mask key
    val l = Array.sub (buckets,i)
    val size = Array.length buckets

    fun updateTable entry l = 
      case (!entry) of 
	NONE => 
	let
	  val _ = Array.update(buckets, i, l)
	  val _ = table := {buckets=buckets, mask=mask, numItems=numItems+1}
	in
	  if (numItems >= 4 * size) then 
	    growTableOne(table,  (growRatio size) * size)
	  else 
	    ()
	end
      | _ => ()

    fun look l =
      case l of
	EMPTY_ONE =>
	let val entry = ref NONE
        in (entry, B_ONE(key,entry,l))
        end
      | (B_ONE(h, v, rest)) =>
	if key=h then
	  case !v of 
	    NONE => 
              let val (entry,rest') = look rest
              in (entry,B_ONE(h,v,rest'))
              end
	  | SOME(_,t1,t2) => 
            if (TimeStamps.isSplicedOut t1) then
              let val (entry,rest') = look rest
              in (entry,B_ONE(h,v,rest'))
              end
            else if Modref.isOutOfFrame(t1,t2) then 
              let val (entry,rest') = look rest
              in (entry,B_ONE(h,v,rest'))
              end
	    else (v,l) 
	else 
          let val (entry,rest') = look rest
          in (entry,B_ONE(h,v,rest'))
          end
    val (entry,l') = look l
    val () = updateTable entry l'
  in
    entry
  end



fun verifyOne (table:'a memotable_one) : unit =
  let
    val {buckets, mask, numItems} = !table
    val n = ref 0

    fun length b = 
      case b of
        EMPTY_ONE => 0
      | B_ONE(_,_,rest) => 1 + length rest

    val n = Array.foldr (fn (b,n) => length b + n) 0 buckets
    val _ = if (n = numItems) then ()
	    else print ("Oops, n = " ^ Int.toString n ^ " numItems = " ^ Int.toString numItems ^ "\n")
  in
    ()
  end

fun numItemsOne (table: 'a memotable_one) = 
  let
    val {buckets, mask, numItems} = !table
  in
    numItems
  end
end
