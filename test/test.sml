(*
fun f (p,a,b) = if p then a else b
val x:int $C=1
val y=2
val z=f (true,x,y)
*)
(*
fun id x = x
val add = op+
val x:int $C=1
val y=2
val z=add(x,y)
*)
datatype 'a list = $C Nil | Cons of 'a * 'a list $C
val tuple : int $C * int $S = (1,2)
and l = Cons(1, Cons (2, Cons (3:int $C, Nil)))

val add (*: (int $C * int $C) -> $C int $C*) = op+
fun inc x = add(x,1) 

fun map f l =
  case l of
    Nil => Nil
  | Cons (h,t) => Cons (f h, map f t) 

val l = map inc l

val a = 1
val b:int $C = 2
val c:int = a+b

fun test (i:bool $C) = if i then inc 1 else 2
fun test (i:bool $S) = if i then inc 1 else 2

fun split f l =
  case l of
    Nil => (Nil, Nil)
  | Cons(h,t) => let
      val x = f h
      val (a,b) = split f t
      in if x then
           let val a' = Cons (h,a)
           in (a', b) end
         else
           let val b' = Cons (h,b)
           in (a, b') end
    end


