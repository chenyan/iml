(******************************************************************************
 **
 ** Copyright 2005 Umut A. Acar
 **
 ** Written by Umut A. Acar, Guy Blelloch, Kanat Tangwongsan.
 **
 ** This program is distributed under the terms of the GNU General
 ** Public License as published by the Free Software Foundation.  See
 ** the LICENSE file for the terms and conditions.
 **
 ***************
 ** Description: The interface for the order-maintenance data structure
 **********************************************************************)
signature ORDERED_LIST =
sig

  eqtype t
  exception badNode
  val init : unit -> t
  val special: t
	       
  val size : t -> int
  val add : t -> t
  val getNext : t -> t option
		     
  (**
   ** spliceOut(start, stop) Splice out the nodes between start and
   ** stop (excluding both).  
   ** Return the # stamps deleted. 
   **)
  val spliceOut: t*t -> int

  (** Return true if spliced out, return false otherwise. **)
  val isSplicedOut: t -> bool
  val isSpecial: t -> bool

  val setInv: t * (unit ->unit) -> unit 
  val getInv: t  -> (unit ->unit)
  val addToInv: t * (unit ->unit) -> unit 

  (** Compare two nodes. **)
  val compare: t*t -> order

  val app: (t -> unit) -> t -> unit 
  val toString: t -> string
  val assert: t -> unit
end
