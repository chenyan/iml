
open Normal

val mapper = fn s => Box.new (s,1)
val reducer = (Box.new (Box.new "",0), 
            fn (acc,cur) => let
                val (_,acc) = Box.valueOf acc
                val (key,v) = Box.valueOf cur
              in Box.new (key,acc+v) end)
val wordcount : string Box.t block -> (string Box.t * int) Box.t block = mapreduce mapper reducer

fun printresult l = print (Int.toString (length l)^"\n")
(*
  case l of
    Nil => print "\n"
  | Cons(h,t) => 
    let
      val (key,v) = Box.valueOf h
    in
      (print ("(" ^ Box.valueOf key ^ "," ^ (Int.toString v) ^ ")");
       printresult t)
    end
*)
fun readfile strm = 
let
  fun getLine () = 
      case TextIO.inputLine strm of
          NONE => NONE
        | SOME line => SOME (String.tokens Char.isSpace line)

  fun getInitialInput nline acc =
      if nline>1 then acc
      else let
          val words = getLine()
      in case words of
             NONE => acc
           | SOME words => getInitialInput (nline+1) (toBlock words acc)
      end

  val words = getInitialInput 1 Nil
  val result = wordcount words
  val _ = printresult result

  val out = TextIO.openOut "normal_time.out"

  fun insertWords words nline =
      let
          val new_words = getLine()
          val _ = case new_words of
                      NONE => ()
                    | SOME new_words => let
                        val words = toBlock new_words words
                        val (result,_) = runexp out nline (wordcount,words)
                    in (printresult result; insertWords words (nline+1)) end
      in
          ()
      end
in
  insertWords words 1
end

val _ =
  (case CommandLine.arguments () of
    file::nil =>
      let
        val strm = TextIO.openIn file
      in
         readfile strm
         handle x => (TextIO.closeIn strm; raise x);
         TextIO.closeIn strm;
         true
       end
   | _ => (print "Usage = .... < file name >\n"  ; false))
  handle IO.Io {name, function, cause} => 
       (print (name^","^exnMessage cause^"\n");
        false)




