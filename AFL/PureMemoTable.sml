(**********************************************************************
 ** PureMemoTable.sml
 **
 ** Copyright 2005 Umut A. Acar
 **
 ** Written by Umut A. Acar and Guy Blelloch
 **
 ** This program is distributed under the terms of the GNU General
 ** Public License as published by the Free Software Foundation.  See
 ** the LICENSE file for the terms and conditions.
 **
 ***************
 ** Description: An implementation of pure memo tables.
 **********************************************************************)

structure PureMemoTable: PURE_MEMO_TABLE = 
struct

  val initSize = 4  (* Must be a power of 2 *)
  val initMask = (Word.fromInt initSize - 0w1)

  type 'a entry = ('a * TimeStamps.t list) option ref
  datatype 'a bucket = EMPTY | B of (int list * 'a * 'a bucket)

  type 'a memotable = {buckets : 'a entry bucket array,
		       mask : word,
		       numItems : int} ref

  type 'a t = 'a memotable

  fun new () = ref {buckets = Array.array(initSize,EMPTY),
		    mask = initMask,
		    numItems = 0} 

  exception InternalError

  val MASK_EVEN = 0wx55555554  (* Even bits are one *)
  val MASK_ODD = 0wx2aaaaaaa   (* Odd bits are one *)

  fun hash mask l = 
    case l of 
      nil => raise InternalError
    | h::nil => 
      let
        val w = Word.fromInt h
	val i = Word.toIntX(Word.andb(w, mask))
      in
	  i
      end

    | h1::h2::_ => 
      let
        val w1 = Word.fromInt h1
        val w2 = Word.fromInt h2
	val w = Word.andb (w1,MASK_EVEN) + Word.andb(w2,MASK_ODD)
	val i = Word.toIntX(Word.andb(w, mask))
      in
	i
      end

  fun listEq (a,b) =   
    let
      fun toString l = List.foldr (fn (k,s) => (Int.toString k ^ "," ^ s)) "" l

      fun eq (a,b) =  
	case (a,b) of
	  (nil,nil) => true
	| (ha::ta,hb::tb) => ha=hb andalso eq(ta,tb)
	| _ => false
     in eq (a,b) end

  fun growTable (table : 'a memotable, newSize : int) : unit =
    let
      val {buckets, mask, numItems} = !table
      val nDeleted = ref 0
      val newArray = Array.array (newSize, EMPTY)
      val newMask = (Word.fromInt newSize - 0w1)

      fun lengthOf b = 
        case b of
          EMPTY => 0
        | B(_,_,t) => 1 + lengthOf t
     
      fun copy b = 
        let
          fun clean l = List.foldr (fn (t, clean) => (if TimeStamps.isSplicedOut t then clean else t::clean)) [] l
         
	  fun c EMPTY = ()
	    | c (B(h, e, rest)) = 
	    let

	      val i = hash newMask h

	      fun doCopy () = 
		let 
		  val l = Array.sub(newArray, i) 
		in
		  Array.update (newArray, i, B(h, e, l))
		end

	      val _ = c rest
		
	    in
	      case (!e) of
		NONE => doCopy ()
	      | SOME(v,tl) => 
		let val tl' = clean tl 
		in case tl' of
		     nil => (nDeleted := !nDeleted +1)
		   | _ => (e := SOME(v,tl'); doCopy ())
		end
	    end
	in
	  c b
	end
     val _ = Array.app copy buckets
   in
     table := {buckets=newArray, mask=newMask, numItems=numItems-(!nDeleted)}
   end

  fun find (table : 'a memotable, key : int list, time: TimeStamps.t) : 'a entry = 
    let
      val {buckets,mask,numItems} = !table
      val i = hash mask key
      val l = Array.sub (buckets,i)
      val size = Array.length buckets

      val nDeleted = ref 0

      fun updateTable entry l = 
	  let
	    val _ = Array.update(buckets, i, l)
	  in
            case (!entry) of 
	      NONE => 
	      let
		val n = numItems + 1
		val _ =	table := {buckets=buckets, mask=mask, numItems=n}
	      in
		if (n >= 2 * size) then 
		  growTable(table, 4 * size)
		else 
		  ()
	      end
	    | _ => ()
	  end


      fun look l =
	case l of
	  EMPTY =>
	    let val entry = ref NONE
            in (entry, B(key,entry,l))
            end
	| (B(k, v, rest)) =>
	    if listEq (key,k) then
	      case !v of 
		 NONE => (v,l)
	       | SOME(x,tl) => 
		 let val _ = v := SOME(x, time::tl) 
		 in (v,l)
		 end
	    else 
              let val (entry,rest') = look rest
              in (entry,B(k,v,rest'))
              end
      val (entry,l') = look l

      val () = updateTable entry l'
    in
      entry
    end

   fun numItems (table: 'a memotable) = 
     let
	 val {buckets, mask, numItems} = !table
     in
	 numItems
     end

end
