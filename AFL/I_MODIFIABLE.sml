(**********************************************************************
 ** I_MODIFIABLE.sml
 **
 ** Copyright 2005 Umut A. Acar
 **
 ** Description: The interface for imperative modifiable references
 **********************************************************************)
signature I_MODIFIABLE =
sig
  eqtype 'a modref
  type 'a t = 'a modref

  type time = TimeStamps.t

  val init : unit -> unit

 (**********************************************************************
  ** Standard operations with modifiables. 
  **********************************************************************)
  val allocMod: ('a*'a -> bool) -> 'a modref
  val allocEqMod: unit -> ''a modref
  val modFromVal: ('a*'a -> bool) -> 'a -> 'a modref 
  val read : 'a modref  -> ('a -> unit) -> unit
  val write : 'a modref -> 'a -> unit
  val keyOfMod : 'a modref -> int
(*
  val mkMemoPure : unit -> (int list) -> (unit -> 'a) -> 'a
  val mkMemo : unit -> (int list) -> (unit -> 'a) -> 'a
*)
  val modToString : ('a -> string)  -> 'a modref -> string
 (**********************************************************************
  ** Meta-machine operations
  **********************************************************************)
  val change: 'a modref -> 'a -> unit
  val deref : 'a modref -> 'a
  val propagate : unit -> int
end

