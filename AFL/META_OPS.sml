(**********************************************************************
 ** META_OPS.sml
 **
 ** Copyright 2005 Umut A. Acar
 **
 ** Written by Umut A. Acar
 **
 ** This program is distributed under the terms of the GNU General
 ** Public License as published by the Free Software Foundation.  See
 ** the LICENSE file for the terms and conditions.
 **
 ***************
 ** Description: The signature for the meta operations
 **********************************************************************)
signature META_OPS = 
sig
   type 'a modref = 'a Modref.t

   val init: unit -> unit
   val restart: unit -> unit
   val finish: unit -> unit
   val change: 'a Box.t modref -> 'a Box.t -> unit
   val change_force: 'a modref -> 'a -> unit
   val change': ('a * 'a -> bool) -> 'a modref  -> 'a -> unit
   val deref : 'a modref -> 'a
   val hasNoReads: 'a modref -> bool
   val propagate : unit -> int
   val numTimeStamps: unit -> int
   val printStats: unit -> unit
end
