datatype 'a list = $C Nil | Cons of 'a * 'a list $C
val l = Cons(1, Cons (2, Cons (3:int $C, Nil)))

val Cons(h,t) = l
val s = h+1
val Nil = l

(*
val a = 1
val b:int $C = 2
val c:int = a+b

fun test (i:bool $C) = if i then inc 1 else 2
fun test (i:bool $S) = if i then inc 1 else 2

fun split f l =
  case l of
    Nil => (Nil, Nil)
  | Cons(h,t) => let
      val x = f h
      val (a,b) = split f t
      in if x then
           let val a' = Cons (h,a)
           in (a', b) end
         else
           let val b' = Cons (h,b)
           in (a, b') end
    end
*)

