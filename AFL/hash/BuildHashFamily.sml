(********************************************************************** 
 ** BuildHashFamily.sml
 **
 ** Copyright 2005 Umut A. Acar
 **
 ** Written by Umut A. Acar.
 **
 ** This program is distributed under the terms of the GNU General
 ** Public License as published by the Free Software Foundation.  See
 ** the LICENSE file for the terms and conditions.
 **
 ***************
 ** Description: 
 ** Provides a family of k-independent random hash functions.
 ** Each hash function returns an integer between 0 and m.  The family
 ** is indexed by a pair of integers.  Each hash function is a degree k
 ** polynomial whose coefficients are chosen randomly.
 **
 **********************************************************************)

functor BuildHashFamily(val k:int val m: int) : HASH_FAMILY = struct
  open Random

  (* Some constants *)
  val SOME(maxInt) = Int.maxInt
  val SOME(minInt) = Int.minInt

  (* A family of K-way independent random hash function *) 
  val K = k
  val realK = Real.fromInt k

  (* Will return numbers mod M *)
  val M = m

  (* A large prime *)
  val P = 1073741789
  val realP = Real.fromInt P

  (* Internal state for SML random number generator *)
  val state  = Random.rand (minInt,maxInt)

  fun hashFun () = 
    let
      fun coefs i = 
        if i = 0 then  nil
	else (Real.fromInt (Random.randInt state)) :: coefs (i-1)

      val cs = coefs K

      fun hash x = 
	let val realx = Real.fromInt x
	    val (s,_) = foldl (fn (c,(s,i)) =>  ((s + c * Math.pow(realx,i)), i-1.0))
	                      (0.0,realK)
			      cs
	    val smodp = s - (Real.realFloor(s/realP) * realP)
	in
          (Real.trunc smodp)  mod M
	end
    in
      hash
    end

end





