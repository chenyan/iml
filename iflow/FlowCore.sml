
structure FlowCore : FLOW_CORE =
struct
    (* Import *)

    open LVType
    open SMLProgram
    infixr ^^ ^/^
    val error = Error.error
    fun display f = (PrettyPrint.output (TextIO.stdOut, f, 79);
                     TextIO.flushOut TextIO.stdOut)

    type Lsigma = (var list * Constraint.constraint)

    val c = new()
    fun dump' c = print (stateToString c ^ "\n")

    val subtype = unifyTy (add_leq c)
    val unifyTy = unifyTy (add_eq c)
    val equTy = unifyTy
(*
    val equTy = unifyTy (add_eq c)
    val unifyTy = unifyTy (add_leq c)
*)
    val instanceTy : Type * TypeScheme * Lsigma -> unit = instanceTy c (add_eq c)
    val ltTy = ltTy (add_leq c)

    val setType = fn (I,ty) => getType I := !ty
    val getGeneral = fn I => (#1 (getGeneral I), copyTy (getReferTy I))

    fun loopOpt f opt = case opt of
                          NONE => ()
                        | SOME opt => f opt

    (* Core *)

   (* Expressions *)

    fun loopAtExp (mode, atexp) =
      let
        val tau = getType(infoAtExp atexp)
      in
        case atexp of
          SCONAtExp(I, scon) => tau
        | IDAtExp(I, _, longvid) => 
            (setTyvars(I, getTyvars(getRefer I));
             if (Type.equals(tau, getReferTy I)
                 handle Fail str => 
                        (substTy (subst c) tau;
                         substTy (subst c) (getReferTy I);
                         display(PPType.ppType tau ^/^ PPType.ppType (getReferTy I) ^^ text("\n"));
                         Error.warning(I, str);
                         true)
                ) then (* same type modulo level *)
               setType(I, getReferTy I)
             else (* instantiated *)
               instanceTy(tau, getGeneral I, getLsigma (getRefer I)) 
               handle Unify => (dump c; 
                                substTy (subst c) tau;
                                substTy (subst c) (#2 (getGeneral I));
                                display(PPType.ppType tau ^/^ PPType.ppTypeScheme (getGeneral I)); error(I,"instanceTy"));
             setType(I, tau);
             tau)
        | RECORDAtExp(I, exprow_opt) =>
          let
            val rho = case exprow_opt of
                        NONE => Type.emptyRow
                      | SOME exprow => loopExpRow (mode,exprow)
            val tau = Type.fromRowType rho
            (*val _ = getLv tau := Level.Stable*)
            val _ = setType (I, tau)
          in tau end
        | LETAtExp(I, dec, exp) => 
            (loopDec dec; 
             let val tau = loopExp (mode,exp)
             in (setType (I, tau); tau) end)
        | PARAtExp(I, exp) => 
            let val tau = loopExp (mode,exp)
            in (setType (I,tau); tau) end
      end

    and loopExpRow (mode, ExpRow(I, lab, exp, exprow_opt)) =
        let
          val tau = loopExp (mode,exp)
          val rho = case exprow_opt of
                      NONE => Type.emptyRow
                    | SOME exprow => loopExpRow (mode,exprow)
          val rowtype = Type.insertRow(rho,lab,tau)
          val _ = setType (I, tau)
        in
          rowtype
        end

    and loopExp (mode, exp) =
          case exp of
            (ATExp(I, atexp)) => 
              let val tau = loopAtExp (mode,atexp)
              in (setType(I, tau); tau) end
          | (APPExp(I, exp, atexp)) => 
              let
                val f = loopExp (mode,exp)
                val tau' = loopAtExp (mode,atexp)
                val FunType (tau1,tau,mode',lv) = !f
                    handle Bind => error(I,"APPExp bind")
                val _ = unifyLv (add_leq c) (mode',mode) handle Unify => error(I,"only call changeable function in changeable mode")
                val _ = ltTy (lv, tau) handle Unify => error(I,"return type must be higher than mode")
                val _ = unifyTy (tau1,tau') 
                    handle Unify => (dump c;
                           display (text "func:" ^/^ ppExp exp ^/^ text "argument:" ^/^ ppAtExp atexp ^/^ text "\n"); 
                           error(I,"application argument level mismatch"))
                val _ = setType(I,tau)
              in tau end
          | (COLONExp(I, exp, ty)) => 
              let 
                val tau1 = loopExp (mode,exp)
                val tau = getASTTy ty
              in
                equTy (tau1,tau) 
                handle Unify => error(I, "expression does not match level annotation");
                setType (I, tau);
                tau
              end
          | (HANDLEExp(I, exp, match)) => 
              let
                val tau1 = loopExp (mode,exp)
                val tau2 = loopMatch (mode,match)
              in
                setType(I,tau1);
                tau1
              end
          | (RAISEExp(I, exp)) => 
              let val tau = loopExp (mode,exp)
              in setType(I,tau); tau end
          | (FNExp(I, match)) => 
              let val tau = loopMatch (mode,match)
              in 
                setType(I,tau); 
                tau 
              end


    (* Matches *)

    and loopMatch (mode, Match(I, mrule, match_opt)) = 
        let
          val tau = loopMrule (mode,mrule)
        in
          setType(I,tau);
          case match_opt of
            NONE => tau
          | SOME match =>
            let
              val tau2 = loopMatch (mode,match)
            in
              unifyTy (tau,tau2)
              handle Unify => error(I, "level mismatch between different matches");
              tau
            end
        end
    and loopMrule (mode, Mrule(I, pat, exp)) =  (* Fun *)
        let
          val mode' = ref Level.Unknown
          val (tau,_) = loopPat (mode',pat) (* no generlize? *)
          val tau' = loopExp (mode',exp)
          val ty = Type.fromFunType(tau,tau',mode',ref Level.Stable)
          (* TODO: constraint return type if elimination happens, collect Con type from loopPat *)
        in
          ltTy (mode', tau') handle Unify => error(I,"return type must be higher than mode");
          setType(I,ty);
          ty
        end


    (* Declarations *)

    and loopDec dec =
         case dec of
           (VALDec(I, tyvarseq, valbind)) => 
             let
               val _ = loopValBind valbind
             in
               ()               
             end
         | (TYPEDec(I, typbind)) => loopTypBind typbind
         | (DATATYPEDec(I, datbind)) => loopDatBind datbind
         | (DATATYPE2Dec(I, tycon, longtycon)) => ()
         | (ABSTYPEDec(I, datbind, dec)) => (loopDatBind datbind; loopDec dec)
         | (EXCEPTIONDec(I, exbind)) => loopExBind exbind
         | (LOCALDec(I, dec1, dec2)) => (loopDec dec1; loopDec dec2)
         | (OPENDec(I, longstrids)) => ()
         | (EMPTYDec(I)) => ()
         | (SEQDec(I, dec1, dec2)) => (loopDec dec1; loopDec dec2)

    and loopValBind (PLAINValBind(I, pat, exp, valbind_opt)) =
        let
          val check = checkpoint c
          val mode = ref Level.Unknown
          val (tau1,info) = loopPat (mode,pat)
          val tau2 = loopExp (mode,exp)
          val _ = unifyTy (tau2,tau1) handle 
              Unify => error (I,"level mismatch on pattern and expression")
          val _ = case (info,!tau1) of
                    (NONE, _) => ()
(*
                  | (SOME I, FunType _) => 
                    let
                      val lsigma = generalize c check
                    in
                      setLsigma (I, lsigma)
                    end
*)
                  | (SOME I, _) => ()
        in
          setType(I,tau1);
          loopOpt loopValBind valbind_opt
        end
      | loopValBind (RECValBind(I, valbind)) = (loopValBind valbind)

    and loopTypBind (TypBind(I, tyvarseq, tycon, ty, typbind_opt)) =
           (loopOpt loopTypBind typbind_opt)
          
    and loopDatBind (DatBind(I, tyvarseq, tycon, lv, conbind, datbind_opt)) =
        let val tau = getType I
        in
          getLv tau := lv;
          loopConBind (tau,conbind);
          loopOpt loopDatBind datbind_opt
        end

    and loopConBind (tau, ConBind(I, _, vid, ty_opt, conbind_opt)) =
        let
          val tau1 = case ty_opt of
                       NONE => tau
                     | SOME ty =>
                       let
                         val tau' = getASTTy ty
                       in
                         Type.fromFunType(tau',tau,ref Level.Stable,ref Level.Stable)
                       end
        in
          setType(I,tau1);
          loopOpt (fn x => loopConBind(tau,x)) conbind_opt
        end

    and loopExBind (NEWExBind(I, _, vid, ty_opt, exbind_opt)) =
          (loopOpt loopExBind exbind_opt)
      | loopExBind (EQUALExBind(I, _, vid, _, longvid, exbind_opt)) =
          (loopOpt loopExBind exbind_opt)

    (* Patterns *)

    and loopAtPat (mode, atpat) =
        let 
          val tau = getType(infoAtPat atpat)
        in
          case atpat of
            (WILDCARDAtPat(I)) => (tau,NONE)
          | (SCONAtPat(I, scon)) => (tau,NONE)
          | (IDAtPat(I, _, longvid)) => 
               (case peekRefer I of
                  NONE => (tau,SOME I) (* bind a variable *) 
                | SOME I' => (* constructor *) (* Case *)
                    ((*if (Type.equals(tau, getType I')) then (* same type modulo level *)
                       setType(I, getType I')
                     else*)  (* instantiated *)
                       instanceTy(tau, getGeneral I, getLsigma (getRefer I)) 
                       handle Unify => (display(PPType.ppType tau ^/^ PPType.ppTypeScheme (getGeneral I)); error(I,"instanceTy"))
                   ; setTyvars(I, #1(getGeneral I))
                   ; (case peekLv tau of
                        SOME lv => (unifyLv (add_leq c) (lv, mode)
                               handle Unify => error(I,"funtion mode must be higher because of elimination of value"))
                      | NONE => () (* TODO Generalized *))
                   ; (tau,NONE)))
          | (RECORDAtPat(I, patrow_opt)) => 
              let
                val rho = case patrow_opt of
                            NONE => Type.emptyRow
                          | SOME patrow => loopPatRow (mode, patrow)
                val tau = Type.fromRowType rho
              in
                setType(I,tau);
                (tau,NONE)
              end
          | (PARAtPat(I, pat)) => 
              let val (tau,info) = loopPat (mode,pat)
              in (setType(I,tau); (tau,info)) end
        end
                                
    and loopPatRow (mode, DOTSPatRow(I)) = 
          let 
            val RowType rho = !(getType I)
                handle Bind => error (I,"DotsPatRow bind")
          in rho end
      | loopPatRow (mode, FIELDPatRow(I, lab, pat, patrow_opt)) =
        let
          val (tau,_) = loopPat (mode, pat) (* no generalize? *)
          val rho = case patrow_opt of
                      NONE => Type.emptyRow
                    | SOME patrow => loopPatRow (mode, patrow)
        in
          setType(I, tau);
          Type.insertRow(rho,lab,tau)
        end

    and loopPat (mode, pat) =
        let 
          val tau = getType(infoPat pat)
        in
          case pat of 
            (ATPat(I, atpat)) => 
              let val (tau,info) = loopAtPat (mode, atpat)
              in setType(I,tau); (tau,info) end
          | (CONPat(I, _, longvid, atpat)) => (* Case *)
              let
                val (alphas,general) = getGeneral I
                val FunType(tau1,tau2,mode',lv) = !general
                val (tau',info) = loopAtPat (mode, atpat) 
                val empty = ([],TRUE)
              in
                instanceTy (tau',(alphas,copyTy tau1),empty)
                handle Unify => error(I, "level mismatch on constructor application");
                instanceTy (tau, (alphas,copyTy tau2),empty)
                handle Unify => error(I, "constructor type cannot be unified");
                (case peekLv tau of
                   SOME lv => (unifyLv (add_leq c) (lv, mode)
                               handle Unify => error(I,"funtion mode must be higher because of elimination of value"))
                 | NONE => () (* TODO Generalized *));
                setType(I, tau);
                (tau,info)
              end
          | (COLONPat(I, pat, ty)) => 
              let
                val (tau1,info) = loopPat (mode,pat)
                val tau = getASTTy ty
              in
                equTy (tau1, tau) handle Unify => error(I, "pattern does not match level annotation");
                setType(I, tau);
                (tau,info)
              end
          | (ASPat(I, _, vid, ty_opt, pat)) => 
              let
                val (tau1,info) = loopPat (mode,pat)
                val tau = case ty_opt of
                            NONE => tau1
                          | SOME ty =>
                            let
                              val tau = getASTTy ty
                            in
                              unifyTy (tau1,tau)
                              handle Unify => error(I, "pattern does not match level annotation");
                              tau
                            end
              in
                setType(I,tau);
                (tau,info)
              end
        end

    (* Module *)

    (* Structures *)

    fun loopStrDec (DECStrDec(I, dec)) = loopDec dec
      | loopStrDec (STRUCTUREStrDec(I, strbind)) = loopStrBind strbind
      | loopStrDec (LOCALStrDec(I, strdec1, strdec2)) = 
          (loopStrDec strdec1; loopStrDec strdec2)
      | loopStrDec (EMPTYStrDec(I)) = ()
      | loopStrDec (SEQStrDec(I, strdec1, strdec2)) = 
          (loopStrDec strdec1; loopStrDec strdec2)

    and loopStrBind (StrBind(I, strid, strexp, strbind_opt)) =
          (loopStrExp strexp; 
           loopOpt loopStrBind strbind_opt)

    and loopStrExp (STRUCTStrExp(I, strdec)) = loopStrDec strdec
      | loopStrExp (IDStrExp(I, longstrid)) = ()
      | loopStrExp (COLONStrExp(I, strexp, sigexp)) = loopStrExp strexp
      | loopStrExp (SEALStrExp(I, strexp, sigexp)) = loopStrExp strexp
      | loopStrExp (APPStrExp(I, funid, strexp)) = loopStrExp strexp
      | loopStrExp (LETStrExp(I, strdec, strexp)) = (loopStrDec strdec; loopStrExp strexp)

    (* Top-level declarations *)

    and loopTopDec (STRDECTopDec(I, strdec, topdec_opt)) = 
          (loopStrDec strdec;
           loopOpt loopTopDec topdec_opt)
      | loopTopDec (SIGDECTopDec(I, sigdec, topdec_opt)) = loopOpt loopTopDec topdec_opt
      | loopTopDec _ = ()

    (* Programs *)

    fun loopProgram (Program(I, topdec, program_opt)) =
        (loopTopDec topdec;
         loopOpt loopProgram program_opt)


    fun processTopDec topdec =
      (* deep copy type ref *)
      (AST.foreach {prog = topdec, 
                handleI = fn I => case peekType I of
                                NONE => ()
                              | SOME ty => setType(I, copyTy ty),
                handlePat = fn _ => ()};
       loopTopDec topdec;
       dump c;
       (*stabilize c;*)
       (* write result back *)
       AST.foreach {prog = topdec, 
                    handleI = fn I => case peekType I of
                          NONE => ()
                        | SOME ty => 
                            (substTy (subst c) ty;
                             substTy (subst c) ty
                            ),
                    handlePat = fn _ => ()}
      )



end;
