(**********************************************************************
 ** MODIFIABLE.sml
 **
 ** Copyright 2005 Umut A. Acar
 **
 ** Written by Umut A. Acar, Guy Blelloch, and Robert Harper. 
 **
 ** This program is distributed under the terms of the GNU General
 ** Public License as published by the Free Software Foundation.  See
 ** the LICENSE file for the terms and conditions.
 **
 ***************
 ** Description: The interface for modifiable references
 **********************************************************************)

signature MODIFIABLE =
sig
  eqtype 'a modref
  type 'a t = 'a modref
  type changeable
  type time = TimeStamps.t

  val init : unit -> unit
  val restart : unit -> unit

 (**********************************************************************
  ** Standard operations with modifiables. 
  **********************************************************************)
  val empty: unit -> 'a modref
  val new: 'a -> 'a modref 
  val modref: ('a modref -> changeable) -> 'a modref 
  val read : 'a modref  -> ('a -> changeable) -> changeable
  val read_notime : 'a modref  -> ('a -> changeable) -> changeable
  val write : 'a Box.t modref -> 'a Box.t -> changeable
  val write' : ('a * 'a -> bool) -> 'a modref -> 'a -> changeable
  val hasNoReads : 'a modref -> bool

  val profileMemo:  (unit -> int)  -> unit
 (**********************************************************************
  ** Meta-machine operations
  **********************************************************************)
  val change: 'a Box.t modref -> 'a Box.t -> unit
  val change': ('a * 'a -> bool) -> 'a modref -> 'a  -> unit
  val cancel: 'a modref -> unit
  val deref : 'a modref -> 'a
  val propagate : unit -> int
  val propagateUntil :  time -> unit
  val resetPropagation: unit -> unit
  val checkQ : unit -> unit

 (**********************************************************************
  ** Some utilities 
  **********************************************************************)
  val now : time ref
  val finger: time ref
  val insertTime: unit -> time
  val setTime:  time -> unit
  val isOutOfFrame: time*time -> bool
  val numTimeStamps: unit -> int

  val printStats: unit -> unit
  val reportStats: unit -> unit  
end

