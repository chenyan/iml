structure Statistics = 
struct
  (* Set this  to true for space sprofiling *)
  val fRunning = ref true
  val fStatisticsMode = ref false
  
  (** Statistics **)
  val NModrefs = ref 0
  val NReads = ref 0
  val NMemoTables = ref 0
  val NMemoEntries = ref 0

  fun resetCounters () = 
      (NModrefs := 0; 
       NReads := 0; 
       NMemoTables := 0; 
       NMemoEntries := 0)

  fun turnOnStatisticsMode () = 
      (resetCounters ();
       fStatisticsMode := true; 
       fRunning := false)

  fun turnOffStatisticsMode () = 
      (fStatisticsMode := false;
       fRunning := true)

  (** Utility functions **)

  fun running () = !fRunning
  fun inStatisticsMode () = !fStatisticsMode
   
  fun inc r = (r := (!r) + 1)
  fun add (r,i) = (r := (!r) + i)
  fun counterToString r = Int.toString (!r)

  fun doInStatisticsMode f = 
    (fn () =>  if (inStatisticsMode ()) then f () else ())


  fun modref t =  
    TimeStamps.setInv (t, doInStatisticsMode (fn () => inc NModrefs))

  fun memo t getNumItems = 
    TimeStamps.setInv (t, doInStatisticsMode (fn () => (inc NMemoTables; add (NMemoEntries, (getNumItems ())))))

  fun incReadCounter () =  
    inc NReads

  fun statsToString () = 
    ("\n" ^ 
     "Number of modrefs            = " ^ counterToString NModrefs ^ "\n" ^
     "Number of reads              = " ^ counterToString NReads ^ "\n" ^
     "Number of memo tables        = " ^ counterToString NMemoTables ^ "\n" ^
     "Number of memo entries       = " ^ counterToString NMemoEntries)
end
