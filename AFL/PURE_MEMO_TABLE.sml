(**********************************************************************
 ** PURE_MEMO_TABLE.sml
 **
 ** Copyright 2005 Umut A. Acar
 **
 ** Written by Umut A. Acar
 **
 ** This program is distributed under the terms of the GNU General
 ** Public License as published by the Free Software Foundation.  See
 ** the LICENSE file for the terms and conditions.
 **
 ***************
 ** Description: The interface for pure memo tables. 
 **********************************************************************)

signature PURE_MEMO_TABLE = 
sig
  type 'a memotable
  type 'a t = 'a memotable


  type 'a entry = ('a * TimeStamps.t list) option ref

  val new: unit -> 'a t
  val find: 'a t * int list * TimeStamps.t -> 'a entry
  val numItems: 'a t -> int
end
