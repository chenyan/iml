structure Normal =
struct

datatype sbool = Strue | Sfalse
datatype cbool = Ctrue | Cfalse

datatype 'a block = 
  Nil | Cons of 'a * 'a block 
fun less (x,y) = if Int.<(x,y) then Strue else Sfalse

type pair = string Box.t * int

structure Hash = HashFamily(val M=2)

fun length l =
    case l of
        Nil => 0
      | Cons(h,t) => 1 + length t

fun toBlock l acc =
    case l of
        nil => acc
      | h::t => Cons(Box.new h, toBlock t acc)

fun map f l =
  case l of
    Nil => Nil
  | Cons (h,t) => Cons (f h, map f t)

fun lengthLessThan n l =
  case less(n,1) of
    Strue => Cfalse
  | Sfalse =>
      (case l of
         Nil => Ctrue
       | Cons(h,t) => lengthLessThan (n-1) t)

fun reduce id binOp l =
  let
    fun hash h = Hash.hashFun () (Box.indexOf h)
    fun halfList l =
      let
        fun half l = 
          let 
            fun sumRun (v,l) = 
              case l of
                Nil => (v, Nil)
              | Cons(h,t) =>
                let
                  val vh = binOp(v,h)
                in
                  if (hash(h)=0) then
                    (vh,t)
                  else sumRun (vh,t)
                end
          in
            case l of
              Nil => Nil
            | Cons (h,t) => 
              let
                val (v,rest) = sumRun (h,t)
                val tail = half rest
              in 
                Cons(v,tail)
              end
          end
      in
        half l
      end
    
    fun comb l =
      let
        val p = lengthLessThan 2 l
      in
        case p of
          Ctrue =>
          (case l of
            Nil => id
          | Cons(h,_) => binOp (id,h))
        | Cfalse =>
          let
            val r = halfList l
          in
            comb r
          end
      end
  in
    comb l
  end

fun split f l =
  case l of
    Nil => (Nil, Nil)
  | Cons(h,t) => let
      val x = f h
      val (a,b) = split f t
    in if x then
	 let val a' = Cons (h,a)
	 in (a', b) end
       else
	 let val b' = Cons (h,b)
	 in (a, b') end
    end

fun merge (a, b) =
  case (a,b) of
    (Nil,_) => b
  | (_,Nil) => a
  | (Cons(ha,ta), Cons(hb,tb)) =>
    let
      val (k1,_) = Box.valueOf ha
      val (k2,_) = Box.valueOf hb
    in
      if String.<(Box.valueOf k1,Box.valueOf k2) then
        Cons(hb, merge(a,tb))
      else
        Cons(ha, merge(ta,b))
    end
      
fun sort l  =
  let
    fun hash h = Hash.hashFun () (Box.indexOf h)
    fun sort' l =
      case lengthLessThan 2 l of 
        Ctrue => l
      | Cfalse => 
  	let
  	  val (even,odd) = split (fn h => hash(h)=0) l
  	  val even_s = sort' (even)
  	  val odd_s = sort' (odd)
  	in
          merge (even_s,odd_s)
  	end
  in
    sort' l
  end

fun groupby l =
  let
    val l = sort l
    fun take key l acc = 
      case l of
        Nil => Cons(Box.new acc, Nil)
      | Cons(h,t) => 
        let
          val (cur_key,v) = Box.valueOf h
        in
          if Box.valueOf key=Box.valueOf cur_key then 
            take key t (Cons(h, acc))
          else
            Cons(Box.new acc, take cur_key t (Cons(h,Nil)))
        end
    val key = case l of Cons(h,_) => #1(Box.valueOf h)
  in
    take key l Nil
  end

fun groupByList l = 
let
  val l = sort l
  fun take acc (k,l)  = 
    case l of 
      Nil => Cons(Box.new acc, Nil)
    | Cons(h,t) => 
      let 
        val (k',v') = Box.valueOf h
      in
        if (Box.valueOf k = Box.valueOf k') then
          take (Cons(h,acc)) (k',t)
        else
          let
            val rest = take Nil (k',l)
          in
            Cons(Box.new acc, rest)
          end
      end
in
  case l of 
    Nil => Cons(Box.new l,Nil)
  | Cons(h,t) => 
    let 
      val (k,v) = Box.valueOf h
    in
      take Nil (k,l)
    end
end


fun mapreduce mapper reducer input = 
  let
    val (id,binop) = reducer
    val pairs = map mapper input
    val merged : pair Box.t block Box.t block = groupByList pairs
    val result : pair Box.t block = map (fn block => reduce id binop (Box.valueOf block)) merged
  in
    result
  end

(*
val input = Cons("he",Cons("is",Cons("he",Cons("a",Cons("read",Cons("is",Cons("a",Cons("he",Nil))))))))
val result = wordcount input
val _ = map (fn (key,v) => print (key ^ "," ^ (Int.toString v) ^ "\n")) result
*)
end
