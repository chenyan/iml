
signature TRANSLATE =
sig
    (* Import *)

    type VId		= IdsCore.VId
    type TyVar		= IdsCore.TyVar

    type Dec		= GrammarCore.Dec
    type Ty		= GrammarCore.Ty
    type TyVarseq	= GrammarCore.TyVarseq

    type TyVarSet	= StaticObjectsCore.TyVarSet
    type Type		= StaticObjectsCore.Type
    type Env		= StaticObjectsCore.Env
    type Context	= StaticObjectsCore.Context


    (* Export *)

    val transProgram : GrammarProgram.Program -> GrammarProgram.Program

end;
