(**********************************************************************
 ** Meta.sml
 **
 ** Copyright 2005 Umut A. Acar
 **
 ** Written by Umut A. Acar
 **
 ** This program is distributed under the terms of the GNU General
 ** Public License as published by the Free Software Foundation.  See
 ** the LICENSE file for the terms and conditions.
 **
 ***************
 ** Description: Meta operations.
 **********************************************************************)

structure Meta :> META_OPS = 
struct
  type 'a modref = 'a Modref.t
  val change  = Modref.change
  fun change_force m v  = Modref.change' (fn _ => false) m v
  val change' = Modref.change' 

  fun init () = (Box.init (); Modref.init ())
  fun restart () = (Box.init (); Modref.restart ())  

  exception unimp
  fun finish () = raise unimp

  val deref = Modref.deref
  val hasNoReads = Modref.hasNoReads
  val propagate = Modref.propagate
  val numTimeStamps = Modref.numTimeStamps
  val printStats = Modref.printStats
end

