(********************************************************************** 
 ** HASH_FAMILY.sml
 **
 ** Copyright 2005 Umut A. Acar
 **
 ** Written by Umut A. Acar.
 **
 ** This program is distributed under the terms of the GNU General
 ** Public License as published by the Free Software Foundation.  See
 ** the LICENSE file for the terms and conditions.
 **
 ***************
 **
 ** Description: Provides a family of random hash functions.
 **********************************************************************)

signature HASH_FAMILY = 
sig
  (* Create a hash function from integers to integers *) 
  val hashFun: unit ->  int -> int
 
end
