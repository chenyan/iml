(**********************************************************************
 ** LeftistHeap.sml
 **
 ** Copyright 2005 Umut A. Acar
 **
 ** Written by Umut A. Acar and Guy Blelloch
 **
 ** This program is distributed under the terms of the GNU General
 ** Public License as published by the Free Software Foundation.  See
 ** the LICENSE file for the terms and conditions.
 **
 ***************
 ** Description: An implementation of leftist heaps. 
 ** This code mostly copied from Purely Functional Data Structures
 ** (p. 20), by Chris Okasaki.
 **********************************************************************)

functor LeftistHeap (structure Element : sig type t val compare: t*t -> order val isValid: t -> bool end)
    :> PRIORITY_QUEUE where type elt =Element.t =
struct
type elt = Element.t

datatype pqueue = E | T of int * elt * pqueue * pqueue
type t = pqueue

exception Empty

(**********************************************************************
 ** Some utilities
 **********************************************************************)

val DEBUG = false
fun dPrint str = 
  if DEBUG then 
    print str
  else 
    ()


(**********************************************************************
 ** Mainline
 **********************************************************************)

val n = ref 0
fun size _ = !n

fun printSize str = 
  dPrint (str ^ " size  = " ^ Int.toString (!n) ^ "\n")

fun incSize () = (n := (!n)+1)
fun decSize () = (n := (!n)-1)
fun resetSize () = (n := 0)

fun rank E = 0
  | rank (T (r, _, _, _)) = r

fun makeheap (x, a, b) =
  if rank a >= rank b then
    T (rank b + 1, x, a, b)
  else
    T (rank a + 1, x, b, a)

val empty = E

fun isEmpty E = true
  | isEmpty (T _) = false

fun merge (h, E) = h
  | merge (E, h) = h
  | merge (h1 as T (_, x, a1, b1), h2 as T (_, y, a2, b2)) =
    (case Element.compare(x,y) of
       LESS => makeheap (x, a1, merge (b1, h2))
     | _    => makeheap (y, a2, merge (h1, b2)))

fun insert (x,h) = 
  let
    fun ins (x, E) = T (1, x, E, E)
      | ins (x, h as T (r, y, a, b)) =
	(case Element.compare(x,y) of
	   LESS => T (r + 1, x, h, E)
	 | _    => makeheap (y, a, ins (x, b)))
  in
    (incSize(); 
     ins (x,h))
  end

fun findMin E = (NONE,E) 
  | findMin (q as T (_, x, _, _)) = (SOME x,q)

fun deleteMin E = raise Empty
  | deleteMin (T (_, x, a, b)) = 
    (decSize(); 
     (x, merge (a, b)))
end

