
datatype sbool = $S Strue | Sfalse
datatype cbool = $C Ctrue | Cfalse

datatype 'a block = $C
  Nil | Cons of 'a * 'a block $C
fun less (x,y) = if Int.<(x,y) then Strue else Sfalse

type key = string
type value = int
type pair = key * value
fun hash n = 1 (*Hash.hashFun ()*)

fun map f l =
  case l of
    Nil => Nil
  | Cons (h,t) => Cons (f h, map f t)

fun lengthLessThan n l =
  case less(n,1) of
    Strue => Cfalse
  | Sfalse =>
      (case l of
         Nil => Ctrue
       | Cons(h,t) => lengthLessThan (n-1) t)

fun reduce id binOp l =
  let
    fun halfList l =
      let
        fun half l = 
          let 
            fun sumRun (v,l) = 
              case l of
                Nil => (v, Nil)
              | Cons(h,t) =>
                let
                  val vh = binOp(v,h)
                in
                  if (hash(h)=0) then
                    (vh,t)
                  else sumRun (vh,t)
                end
          in
            case l of
              Nil => Nil
            | Cons (h,t) => 
              let
                val (v,rest) = sumRun (h,t)
                val tail = half rest
              in 
                Cons(v,tail)
              end
          end
      in
        half l
      end
    
    fun comb l =
      let
        val p = lengthLessThan 2 l
      in
        case p of
          Ctrue =>
          (case l of
            Nil => id
          | Cons(h,_) => binOp (id,h))
        | Cfalse =>
          let
            val r = halfList l
          in
            comb r
          end
      end
  in
    comb l
  end

fun split f l =
  case l of
    Nil => (Nil, Nil)
  | Cons(h,t) => let
      val x = f h
      val (a,b) = split f t
    in if x then
	 let val a' = Cons (h,a)
	 in (a', b) end
       else
	 let val b' = Cons (h,b)
	 in (a, b') end
    end

fun merge (a, b) =
  case (a,b) of
    (Nil,_) => b
  | (_,Nil) => a
  | (Cons(ha as (k1,_),ta), Cons(hb as (k2,_),tb)) =>
    if String.<(k1,k2) then
      Cons(hb, merge(a,tb))
    else
      Cons(ha, merge(ta,b))
      
fun sort l  =
  let
    fun sort' l =
      case lengthLessThan 2 l of 
        Ctrue => l
      | Cfalse => 
  	let
  	  val (even,odd) = split (fn h => hash(h)=0) l
  	  val even_s = sort' (even)
  	  val odd_s = sort' (odd)
  	in
          merge (even_s,odd_s)
  	end
  in
    sort' l
  end

fun groupby l =
  let
    val l = sort l
    fun take key l acc = 
      case l of
        Nil => Cons(acc, Nil)
      | Cons((cur_key,v),t) => 
          if key=cur_key then 
            take key t (Cons((cur_key,v), acc))
          else
            Cons(acc, take cur_key t (Cons((cur_key,v),Nil)))
    val Cons((key,_),_) = l
  in
    take key l Nil
  end

fun groupByList l = 
let
  val l = sort l
  fun take acc (k,l)  = 
    case l of 
      Nil => Cons(acc, Nil)
    | Cons(h,t) => 
      let 
        val (k',v') = h
      in
        if (k=k') then
          take (Cons(h,acc)) (k',t)
        else
          let
            val rest = take Nil (k',l)
          in
            Cons(acc, rest)
          end
      end
in
  case l of 
    Nil => Cons(l,Nil)
  | Cons(h,t) => 
    let 
      val (k,v) = h
    in
      take Nil (k,l)
    end
end

fun mapreduce mapper reducer input = 
  let
    val (id,binop) = reducer
    val pairs = map mapper input
    val merged = groupby pairs
    val result = map (fn block => reduce id binop block) merged
  in
    result
  end


