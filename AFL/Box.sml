(**********************************************************************
 ** Box.sml
 **
 ** Copyright 2005 Umut A. Acar
 **
 ** Written by Umut A. Acar
 **
 ** This program is distributed under the terms of the GNU General
 ** Public License as published by the Free Software Foundation.  See
 ** the LICENSE file for the terms and conditions.
 **
 ***************
 ** Description: An implementation of boxed values with integer ids.
 **********************************************************************)

structure Box : BOXED_VALUE = 
struct
   
  type index = int
  type 'a box = int * 'a
  type 'a t = 'a box


  val next = ref (0)
  fun getNext () = (next := (!next) + 1; (!next))

  fun init () = next := ~1

  
  fun new v = (getNext(), v)
  fun fromInt i = (i,i)

  fun newIndex () = getNext ()
  fun mkBox (id, v) = (id, v)

  fun  fromOption ob =
    case ob of
      NONE => (~1,ob)
    | SOME (i,_) => (i,ob)
 
  fun eq (a as (ka,_),b as (kb,_)) = (ka=kb)
  fun valueOf (_,v) = v
  fun indexOf (k,_) = k
   
  fun toString (k,_) = Int.toString k
end




