(**********************************************************************
 ** Comb.sml
 **
 ** Copyright 2005 Umut A. Acar
 **
 ** Written by Umut A. Acar
 **
 ** This program is distributed under the terms of the GNU General
 ** Public License as published by the Free Software Foundation.  See
 ** the LICENSE file for the terms and conditions.
 **
 ***************
 ** Description: An implementation of the combinator library.
 **********************************************************************)

structure Comb :> COMBINATORS = 
struct

type 'a modref = 'a Modref.t
type changeable = Modref.changeable	
type 'a cc = 'a modref -> changeable
  
exception NYI
exception InternalError

(**********************************************************************
 ** Debugging stuff
 **********************************************************************)
val NO_DEBUG = true
val DEBUG = false andalso (not NO_DEBUG)
val DEEP_DEBUG = false andalso (not NO_DEBUG)

fun dPrint f = if (DEBUG) then print(f() ^ "\n") else ()

(**********************************************************************
 ** Some short hands
 **********************************************************************)
   fun newTablePure () =
       let
           val t = PureMemoTable.new ()
           val _ = Modref.profileMemo (fn () => PureMemoTable.numItems t)
       in
           t
       end


   fun newTable () =
       let
           val t = MemoTable.new ()
           val _ = Modref.profileMemo (fn () => MemoTable.numItems t)
       in
           t
       end
  fun newTableOne () =
       let
           val t = MemoTable.newOne ()
           val _ = Modref.profileMemo (fn () => MemoTable.numItemsOne t)
       in
           t
       end

(*fun newTablePure () =  PureMemoTable.new ()
fun newTable () = MemoTable.new ()
fun newTableOne () = MemoTable.newOne () *)

   fun delete table ts =
     if Statistics.running () then
         MemoTable.delete table ts
     else
         ()

   fun deleteOne table ts =
     if Statistics.running () then
         MemoTable.deleteOne table ts
     else
         ()

   fun deletePure table ts =
     if Statistics.running () then
         MemoTable.deleteOne table ts
     else
         ()
(*
fun delete table ts = MemoTable.delete table ts
fun deleteOne table ts = MemoTable.deleteOne table ts
fun deletePure table ts = MemoTable.deleteOne table ts
*)
(**********************************************************************
 ** Mainline
 **********************************************************************)
fun write x d = (Modref.write d x)
fun write' eq x d = (Modref.write' eq d x)
fun write_eq x d = (Modref.write' (fn (a,b) => a=b) d x)
fun write_force x d = (Modref.write' (fn (a,b) => false) d x)

val iwrite = Modref.write
val iwrite' = Modref.write' 
fun iwrite_eq' d x = Modref.write' (fn(a,b) => a=b) d x

fun read (r, recv) d = Modref.read r (fn x => recv x d)
fun read_notime (r, recv) d = Modref.read_notime r (fn x => recv x d)
fun read_empty (r, recv) d = Modref.read_notime r (fn x => recv x d)
val iread = Modref.read
fun icopy dest src = Modref.read src (fn data =>iwrite dest data)

val modref = Modref.modref
val new = Modref.empty
val fromVal = Modref.new

(** Insert the entry into the memo table at r **)
fun insert delete (mref,entry,(t1,t2,nt1o)) = 
  let
    val (start,stop) = 
	case nt1o of 
	  NONE =>  
	  let val t3 = Modref.insertTime ()
	  in (t3,t3)
	  end
	| SOME(nt1) =>
	  if (TimeStamps.compare (nt1,t2)=LESS) then (nt1,t2)
	  else let val t3 = Modref.insertTime ()
	       in (t3,t3)
	       end
    val _ = TimeStamps.addToInv (stop, fn () => delete (start,stop)) 
  in
    mref := SOME(entry, start,stop)
  end

fun spliceIn (t1,t2) = 
  let
    val _ = TimeStamps.spliceOut (!(Modref.now),t1)
    val _ = Modref.now := t1
  in
    ()
  end
	
fun update (g, t2) = (g (); Modref.propagateUntil t2)


fun liftPure pad f key =
  let
    fun run_memoized (mref,t) = 
      let
	val v = f()
        val _ =  mref := SOME(v,[t]) 
      in v end

    val t = Modref.insertTime ()
    val mref = PureMemoTable.find (pad, key, t)
  in
    case !mref of
      NONE => run_memoized (mref,t)
    | SOME(v,_) => v
  end

fun mkLiftOptimized a b  = 
  let
    val pad = newTableOne ()

    fun lift x y = 
      let
	val mref = MemoTable.findOne (pad, [Box.indexOf x], !Modref.now)

	fun insert (v, t1,t2) = 
	  let
	    val delete = MemoTable.deleteOne (pad, [Box.indexOf x])
	    val SOME(nt1) = TimeStamps.getNext t1
	    val _ = TimeStamps.addToInv (t2, fn () => delete (nt1,t2))
	  in
	    mref := SOME(v, nt1, t2)
	  end
      in
	case !mref of
	  NONE => 
	  let 
	    val xx = a x 
	    val t1 = !(Modref.now)
	    val r = b lift xx y
	    val t2 = !(Modref.now)
	    val _ = insert ((xx,y,r),t1,t2)
	  in
	    r
	  end

	| SOME((xx,y_old,r),t1,t2) =>
	  if (y = y_old) then
	    (spliceIn (t1,t2); Modref.propagateUntil t2; r)
	  else
	    let 
	      val t1 = !(Modref.now)
	      val r_new = b lift xx y 
	      val t2 = !(Modref.now)
	      val _ = insert ((xx,y,r_new),t1,t2)
	    in
	      r_new
	    end
      end
  in
    lift
  end


fun mkLiftOptimized' eq a b  = 
  let
    val pad = newTableOne ()

    fun memoTimes (t1,t2,nt1o) = 
        case nt1o of 
	  NONE =>  
	  let val t3 = Modref.insertTime ()
	  in (t3,t3)
	  end
	| SOME(nt1) =>
	  if (TimeStamps.compare (nt1,t2)=LESS) then (nt1,t2)
	  else let val t3 = Modref.insertTime ()
	       in (t3,t3)
	       end

    fun lift x y = 
      let
	val mref = MemoTable.findOne (pad, [Box.indexOf x], !Modref.now)

	fun insert (v, t1,t2) = 
	  let
	    val delete = MemoTable.deleteOne (pad, [Box.indexOf x])
	    val (start,stop) = memoTimes (t1,t2, TimeStamps.getNext t1)
	    val _ = TimeStamps.addToInv (stop, fn () => delete (start,stop))
	  in
	    mref := SOME(v, start, stop)
	  end
      in
	case !mref of
	  NONE => 
	  let 
	    val xx = a x   (* Assumption: a does not allocate time stamps *)
	    val t1 = !(Modref.now)
	    val r = b lift xx y
	    val t2 = !(Modref.now)
	    val _ = insert ((xx,y,r),t1,t2)
	  in
	    r
	  end

	| SOME((xx,y_old,r),t1,t2) =>
	  if (eq(y,y_old)) then
	    (spliceIn (t1,t2); Modref.propagateUntil t2; r)
	  else
	    let                          (* treat as though no memo hit.  monoticity seems key *)
	      val t1 = !(Modref.now)
	      val r_new = b lift xx y 
	      val t2 = !(Modref.now)
	      val _ = insert ((xx,y,r_new),t1,t2)
	    in
	      r_new
	    end
      end
  in
    lift
  end

(**********************************************************************
 ** General Lift operators
 **********************************************************************)
fun generalLift0 (find,delete)  pad f key =
  let
    fun run_memoized (f,mref) = 
      let
	val t1 = !(Modref.now)
	val v = f()
	val t2 = !(Modref.now)
	val nt1o = TimeStamps.getNext t1
        val delete =  delete (pad,key)
        val _ = insert delete (mref,v,(t1,t2,nt1o))
      in
	v
      end
 
    val mref = find (pad, key, !Modref.now)
  in
    case !mref of
      NONE => run_memoized (f, mref)
    | SOME(v,t1,t2) =>(spliceIn (t1,t2);
                       update (fn () => (), t2);
                       v) 
  end

fun generalLift1 (find,delete) pad wrb f (key,b) = 
  let
    fun run_memoized (f,b,mref) = 
      let
        val rb = modref (wrb b)
	val t1 = !(Modref.now)
	val v = f(rb)
	val t2 = !(Modref.now)
	val nt1o = TimeStamps.getNext t1
        val delete =  delete (pad,key)
        val _ = insert delete (mref,(v,rb),(t1,t2,nt1o))
      in
	v
      end
    val mref = find (pad,key,!Modref.now)
  in 
    case !mref of
      NONE => run_memoized (f,b,mref)
    |  SOME((v,rb),t1,t2) => (spliceIn (t1,t2); 
			      update (fn () => wrb b rb, t2); 
			      v) 
  end

fun generalLift2 (find, delete) pad (wrb,wrc) f (key,b,c) = 
  let
    fun run_memoized (f,b,c,mref) = 
      let
        val rb = modref (wrb b)
        val rc = modref (wrc c )
	val t1 = !(Modref.now)
	val v = f(rb,rc)
	val t2 = !(Modref.now)
	val nt1o = TimeStamps.getNext t1
        val delete =  delete (pad,key)
        val _ = insert delete (mref,(v,rb,rc),(t1,t2,nt1o))
      in
	v
      end

    val mref = find (pad,key,!Modref.now)

  in 
    case !mref of
      NONE =>  run_memoized (f,b,c,mref) 
    |  SOME((v,rb,rc),t1,t2) => (spliceIn (t1,t2); 
				 update (fn () => (wrb b rb; wrc c rc), t2); 
				 v)
  end
 
fun generalLift3 (find, delete) pad (wrb,wrc,wrd) f (key,b,c,d) = 
  let
    fun run_memoized (f,b,c,d,mref) = 
      let
	val (rb,rc,rd) = (modref (wrb b), modref (wrc c), modref (wrd d))
	val t1 = !(Modref.now)
	val v = f(rb,rc,rd)
	val t2 = !(Modref.now)
	val nt1o = TimeStamps.getNext t1
        val delete =  delete (pad,key)
        val _ = insert delete (mref,(v,rb,rc,rd),(t1,t2,nt1o))
      in
	v
      end

    val mref = find (pad,key,!Modref.now)

  in 
    case !mref of
      NONE =>  run_memoized (f,b,c,d,mref) 
    |  SOME((v,rb,rc,rd),t1,t2) => (spliceIn (t1,t2); 
				    update (fn () => (wrb b rb; wrc c rc; wrd d rd), t2); 
				    v)
  end

fun generalLift4 (find, delete) pad (wrb,wrc,wrd,wre) f (key,b,c,d,e) = 
  let
    fun run_memoized (f,b,c,d,e,mref) = 
      let
	val (rb,rc,rd,re) = (modref (wrb b), modref (wrc c), 
				   modref (wrd d), modref (wre e))
	val t1 = !(Modref.now)
	val v = f(rb,rc,rd,re)
	val t2 = !(Modref.now)
	val nt1o = TimeStamps.getNext t1
        val delete =  delete (pad,key)
        val _ = insert delete (mref,(v,rb,rc,rd,re),(t1,t2,nt1o))
      in
	v
      end

    val mref = find (pad,key,!Modref.now)

  in 
    case !mref of
      NONE =>  run_memoized (f,b,c,d,e,mref) 
    |  SOME((v,rb,rc,rd,re),t1,t2) => (spliceIn (t1,t2); 
				       update (fn () => (wrb b rb; wrc c rc; wrd d rd; wre e re), t2); 
				       v)
  end

fun generalLiftCC0 (find, delete) pad (wrd) f (key) = 
  let
    fun run_memoized (f,mref) = 
      let
	val t1 = !(Modref.now)
	val v = modref (f ())
	val t2 = !(Modref.now)
	val nt1o = TimeStamps.getNext t1
        val delete = delete (pad,key)
        val _ = insert delete (mref,v,(t1,t2,nt1o))
      in
	v
      end

    val mref = find (pad,key,!Modref.now)
  in 
    case !mref of
      NONE =>  read_empty (run_memoized (f,mref), wrd)
    |  SOME(v,t1,t2) => (spliceIn (t1,t2); 
			 update (fn () => (),t2); 
			 read_empty (v, wrd))
  end

fun generalLiftCC1 (find, delete) pad (wrb,wrd) f (key,b) = 
  let
    fun run_memoized (f,b,mref) = 
      let
        val rb = modref (wrb b)
	val t1 = !(Modref.now)
	val v = modref (f rb)
	val t2 = !(Modref.now)
	val nt1o = TimeStamps.getNext t1
        val delete = delete (pad,key)
        val _ = insert delete (mref,(v,rb),(t1,t2,nt1o))
      in
	v
      end

    val mref = find (pad,key,!Modref.now)
  in 
    case !mref of
      NONE =>  read_empty (run_memoized (f,b,mref), wrd)
    | SOME((v,rb),t1,t2) => (spliceIn (t1,t2); 
			     update (fn () => wrb b rb,t2); 
			     read_empty (v, wrd))
  end

fun generalLiftCC2 (find, delete) pad (wrb,wrc,wrd) f (key,b,c) = 
  let
    fun run_memoized (f,b,c,mref) = 
      let
        val rb = modref (wrb b)
        val rc = modref (wrc c )
	val t1 = !(Modref.now)
	val v = modref (f (rb,rc))
	val t2 = !(Modref.now)
	val nt1o = TimeStamps.getNext t1
        val delete = delete (pad,key)
        val _ = insert delete (mref,(v,rb,rc),(t1,t2,nt1o))
      in
	v
      end

    val mref = find (pad,key,!Modref.now)
  in 
    case !mref of
      NONE =>  read_empty (run_memoized (f,b,c,mref), wrd)
    |  SOME((v,rb,rc),t1,t2) => (spliceIn (t1,t2); 
				 update (fn () => (wrb b rb; wrc c rc),t2); 
				 read_empty (v,wrd))
  end

fun generalLiftCC3 (find, delete) pad (wra,wrb,wrc,wrd) f (key,a,b,c) = 
  let
    fun run_memoized (f,a,b,c,mref) = 
      let
  	val ra = modref (wra a)
        val rb = modref (wrb b)
        val rc = modref (wrc c )
	val t1 = !(Modref.now)
	val v = modref (f (ra,rb,rc))
	val t2 = !(Modref.now)
	val nt1o = TimeStamps.getNext t1
        val delete =  delete (pad,key)
        val _ = insert delete (mref,(v,ra,rb,rc),(t1,t2,nt1o))
      in
	v
      end

    val mref = find (pad,key,!Modref.now)
  in 
    case !mref of
      NONE =>  read_empty (run_memoized (f,a,b,c,mref), wrd)
    |  SOME((v,ra,rb,rc),t1,t2) => 
       (spliceIn (t1,t2); 
	update (fn () => (wra a ra;wrb b rb; wrc c rc),t2); 
	read_empty (v,wrd))
  end

fun generalLiftCC4 (find, delete) pad (wra,wrb,wrc,wrd,wrw) f (key,a,b,c,d) = 
  let
    fun run_memoized (f,a,b,c,d,mref) = 
      let
     	val ra = modref (wra a)
        val rb = modref (wrb b)
        val rc = modref (wrc c)
	val rd = modref (wrd d)
	val t1 = !(Modref.now)
	val v = modref (f (ra,rb,rc,rd))
	val t2 = !(Modref.now)
	val nt1o = TimeStamps.getNext t1
        val delete = delete (pad,key)
        val _ = insert delete (mref,(v,ra,rb,rc,rd),(t1,t2,nt1o))
      in
	v
      end
	   
    val mref = find (pad,key,!Modref.now)
  in 
    case !mref of
      NONE =>  read_empty (run_memoized (f,a,b,c,d,mref), wrw)
    |  SOME((v,ra,rb,rc,rd),t1,t2) => 
       (spliceIn (t1,t2); 
	update (fn () => (wra a ra;wrb b rb; wrc c rc; wrd d rd),t2); 
	read_empty (v,wrw))
  end
  
fun generalLiftCC5 (find, delete) pad (wra,wrb,wrc,wrd,wre, wrw) f (key,a,b,c,d,e) = 
  let
    fun run_memoized (f,a,b,c,d,e,mref) = 
      let
     	val ra = modref (wra a)
        val rb = modref (wrb b)
        val rc = modref (wrc c)
	val rd = modref (wrd d)
	val re = modref (wre e)
	val t1 = !(Modref.now)
	val v = modref (f (ra,rb,rc,rd,re))
	val t2 = !(Modref.now)
	val nt1o = TimeStamps.getNext t1
        val delete =  delete (pad,key)
        val _ = insert delete (mref,(v,ra,rb,rc,rd,re),(t1,t2,nt1o))
      in
	v
      end
	   
    val mref = find (pad,key,!Modref.now)
  in 
    case !mref of
      NONE =>  read_empty (run_memoized (f,a,b,c,d,e,mref), wrw)
    |  SOME((v,ra,rb,rc,rd,re),t1,t2) => 
       (spliceIn (t1,t2); 
	update (fn () => (wra a ra;wrb b rb; wrc c rc; wrd d rd; wre e re),t2); 
	read_empty (v,wrw))
  end

(**********************************************************************
 ** mkLifts
 **********************************************************************)
fun generalMkLift0 new lift = 
  let
    val pad = new ()
    fun lifted arg f = lift pad f arg
  in
    lifted
  end

fun generalMkLift1 new lift eqb = 
  let
    val pad = new ()
    val wrb = write' eqb
    fun lifted arg f = lift pad wrb f arg     
  in
    lifted
  end

fun generalMkLift2 new lift (eqb,eqc) = 
  let
    val pad = new ()
    val wrb = write' eqb
    val wrc = write' eqc
    fun lifted arg f = lift pad (wrb,wrc) f arg     
  in
    lifted
  end

fun generalMkLift3 new lift (eqb,eqc,eqd) = 
  let
    val pad = new ()
    val wrb = write' eqb
    val wrc = write' eqc
    val wrd = write' eqd
    fun lifted arg f = lift pad (wrb,wrc,wrd) f arg     
  in
    lifted
  end

fun generalMkLift4 new lift (eqb,eqc,eqd,eqe) = 
  let
    val pad = new ()
    val wrb = write' eqb
    val wrc = write' eqc
    val wrd = write' eqd
    val wre = write' eqe
    fun lifted arg f = lift pad (wrb,wrc,wrd,wre) f arg     
  in
    lifted
  end

fun generalMkLiftCC0 new lift (eqd) = 
  let
    val pad = new ()
    val wrd = write' eqd
    fun lifted arg f = lift pad (wrd) f arg     
  in
    lifted
  end

fun generalMkLiftCC1 new lift (eqb,eqd) = 
  let
    val pad = new ()
    val wrb = write' eqb
    val wrd = write' eqd
    fun lifted arg f = lift pad (wrb,wrd) f arg     
  in
    lifted
  end

fun generalMkLiftCC2 new lift (eqb,eqc,eqd) = 
  let
    val pad = new ()
    val wrb = write' eqb
    val wrc = write' eqc
    val wrd = write' eqd
    fun lifted arg f = lift pad (wrb,wrc,wrd) f arg     
  in
    lifted
  end

fun generalMkLiftCC3 new lift (eqa, eqb,eqc,eqd) = 
  let
    val pad = new ()
    val wra = write' eqa
    val wrb = write' eqb
    val wrc = write' eqc
    val wrd = write' eqd
    fun lifted arg f = lift pad (wra,wrb,wrc,wrd) f arg     
  in
    lifted
  end

fun generalMkLiftCC4 new lift (eqa,eqb,eqc,eqd,eqe) = 
  let 
    val pad = new ()
    val wra = write' eqa
    val wrb = write' eqb
    val wrc = write' eqc
    val wrd = write' eqd
    val wre = write' eqe
    fun lifted arg f = lift pad (wra,wrb,wrc,wrd,wre) f arg     
  in
    lifted
  end

fun generalMkLiftCC5 new lift (eqa,eqb,eqc,eqd,eqe,eqw) = 
  let
    val pad = new ()
    val writers = (write' eqa, write' eqb, 
		   write' eqc, write' eqd, write' eqe, write' eqw)
    fun lifted arg f = lift pad writers f arg     
  in
    lifted
  end

(**********************************************************************
 ** lift operators
 **********************************************************************)
fun lift0_one pad f x  = generalLift0 (MemoTable.findOne,deleteOne) pad f x
fun lift1_one pad wrs f x = generalLift1 (MemoTable.findOne, deleteOne) pad wrs f x
fun lift2_one pad wrs f x = generalLift2 (MemoTable.findOne, deleteOne) pad wrs f x
fun lift3_one pad wrs f x = generalLift3 (MemoTable.findOne, deleteOne) pad wrs f x
fun lift4_one pad wrs f x = generalLift4 (MemoTable.findOne, deleteOne) pad wrs f x
			    
fun lift0 pad f x  = generalLift0 (MemoTable.find, delete) pad f x
fun lift1 pad wrs f x = generalLift1 (MemoTable.find, delete) pad wrs f x
fun lift  pad wrs f x = generalLift1 (MemoTable.find, delete) pad wrs f x
fun lift2 pad wrs f x = generalLift2 (MemoTable.find, delete) pad wrs f x
fun lift3 pad wrs f x = generalLift3 (MemoTable.find, delete) pad wrs f x
fun lift4 pad wrs f x = generalLift4 (MemoTable.find, delete) pad wrs f x


fun liftCC0_one pad wrs f x = generalLiftCC0 (MemoTable.findOne, deleteOne) pad wrs f x
fun liftCC1_one pad wrs f x = generalLiftCC1 (MemoTable.findOne, deleteOne) pad wrs f x
fun liftCC2_one pad wrs f x = generalLiftCC2 (MemoTable.findOne, deleteOne) pad wrs f x
fun liftCC3_one pad wrs f x = generalLiftCC3 (MemoTable.findOne, deleteOne) pad wrs f x
fun liftCC4_one pad wrs f x = generalLiftCC4 (MemoTable.findOne, deleteOne) pad wrs f x
fun liftCC5_one pad wrs f x = generalLiftCC5 (MemoTable.findOne, deleteOne) pad wrs f x

fun liftCC0 pad wrs f x = generalLiftCC0 (MemoTable.find, delete) pad wrs f x
fun liftCC1 pad wrs f x = generalLiftCC1 (MemoTable.find, delete) pad wrs f x
fun liftCC  pad wrs f x = generalLiftCC1 (MemoTable.find, delete) pad wrs f x
fun liftCC2 pad wrs f x = generalLiftCC2 (MemoTable.find, delete) pad wrs f x
fun liftCC3 pad wrs f x = generalLiftCC3 (MemoTable.find, delete) pad wrs f x
fun liftCC4 pad wrs f x = generalLiftCC4 (MemoTable.find, delete) pad wrs f x
fun liftCC5 pad wrs f x = generalLiftCC5 (MemoTable.find, delete) pad wrs f x


(**********************************************************************
 ** mkLifts
 **********************************************************************)
fun mkLiftPure () =  
  let
    val pad = newTablePure ()
    fun lifted arg f = liftPure pad f arg
  in
    lifted
  end

fun mkLift0_one ()  = generalMkLift0 newTableOne lift0_one  
fun mkLift1_one eqs  = generalMkLift1 newTableOne lift1_one eqs
fun mkLift_one eqs  = generalMkLift1 newTableOne lift1_one eqs
fun mkLift2_one eqs  = generalMkLift2 newTableOne lift2_one eqs
fun mkLift3_one eqs  = generalMkLift3 newTableOne lift3_one eqs
fun mkLift4_one eqs  = generalMkLift4 newTableOne lift4_one eqs

fun mkLiftCC0_one eqs  = generalMkLiftCC0 newTableOne  liftCC0_one eqs
fun mkLiftCC1_one eqs  = generalMkLiftCC1 newTableOne liftCC1_one eqs
fun mkLiftCC_one eqs  = generalMkLiftCC1  newTableOne liftCC1_one eqs
fun mkLiftCC2_one eqs = generalMkLiftCC2  newTableOne liftCC2_one eqs
fun mkLiftCC3_one eqs = generalMkLiftCC3  newTableOne liftCC3_one eqs
fun mkLiftCC4_one eqs = generalMkLiftCC4  newTableOne liftCC4_one eqs
fun mkLiftCC5_one eqs = generalMkLiftCC5  newTableOne liftCC5_one eqs

fun mkLift0 () = generalMkLift0 newTable lift0 
fun mkLift1 eqs = generalMkLift1 newTable lift1 eqs
fun mkLift eqs = generalMkLift1 newTable lift1 eqs
fun mkLift2 eqs = generalMkLift2 newTable lift2 eqs
fun mkLift3 eqs = generalMkLift3 newTable lift3 eqs
fun mkLift4 eqs = generalMkLift4 newTable lift4 eqs

fun mkLiftCC0 eqs = generalMkLiftCC0 newTable liftCC0 eqs
fun mkLiftCC1 eqs = generalMkLiftCC1 newTable liftCC1 eqs
fun mkLiftCC eqs  = generalMkLiftCC1 newTable liftCC1 eqs
fun mkLiftCC2 eqs  = generalMkLiftCC2 newTable liftCC2 eqs
fun mkLiftCC3 eqs  = generalMkLiftCC3 newTable liftCC3 eqs
fun mkLiftCC4 eqs  = generalMkLiftCC4 newTable liftCC4 eqs
fun mkLiftCC5 eqs  = generalMkLiftCC5 newTable liftCC5 eqs

end

