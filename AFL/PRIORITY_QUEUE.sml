(**********************************************************************
 ** PRIORITY_QUEUE.sml
 **
 ** Signature for priority queues
 **********************************************************************)

signature PRIORITY_QUEUE =
sig
  type elt
  type t

  exception Empty

  val empty : t
  val isEmpty : t -> bool

  val findMin : t -> elt option * t 
  val insert : elt * t -> t 
  val deleteMin : t -> elt * t 
  val size: t -> int
end
