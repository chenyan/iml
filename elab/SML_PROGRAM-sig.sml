(*
 * (c) Andreas Rossberg 2007
 *
 * Printer for abstract program grammar
 *)

signature SML_PROGRAM =
sig
    type Program = GrammarProgram.Program
    type doc = PrettyPrint.doc

    val dispType : bool ref

    val printSML : Program -> unit
    val ppExp : GrammarCore.Exp -> doc
    val ppAtExp : GrammarCore.AtExp -> doc
end;
