(**********************************************************************
 ** ListTimeStamps.sml
 **
 ** Copyright 2005 Umut A. Acar
 **
 ** Written by Umut A. Acar
 **
 ** This program is distributed under the terms of the GNU General
 ** Public License as published by the Free Software Foundation.  See
 ** the LICENSE file for the terms and conditions.
 **
 ***************
 ** Description: This is a linear-time implementation of time stamps.
 ** It is used for debugging purposes.
 **********************************************************************)
structure TimeStamps : ORDERED_LIST = 
struct

  exception badInput 
  exception badNode
  exception verifyFailed
  exception assertFailed

  val DEBUG = false
  fun dPrint str = 
    if DEBUG then
	print str
    else
	()

  val dummyInvalidator = fn () => ()
 
  type t = (int * (unit -> unit)) ref
 
  type stats = {deleted: int, new: int, inserted:int}
  val counters = ref {deleted=0,new=0,inserted=0}
  
  fun resetStats () = counters := {deleted=0,new=0,inserted=0}

  fun countNew () =  ()
(*
    let
      val {new=n,deleted=d,inserted=i} = !counters
    in
      counters := {deleted=d,new=n+1,inserted=i}
    end
*)
  fun countDelete () =  ()
(*
    let
      val {new=n,deleted=d,inserted=i} = !counters
    in
      counters := {deleted=d-1,new=n,inserted=i}
    end
*)
  fun countInsert () =  ()
(*
    let
      val {new=n,deleted=d,inserted=i} = !counters
    in
      counters := {deleted=d,new=n,inserted=i+1}
    end
*)
  type time = (t list) ref

  val time: t list ref = ref [ ]

  fun eq (ref (i,_),ref (j,_)) = (i=j)

  val special = ref (0,dummyInvalidator)
  fun isSpecial (node:t) = eq(node, special)

  (** time ids **)
  val ID = ref 1
  fun nextID  () = 
    let val i = !ID
    in (ID := i +1; i)
    end
  
  fun new () = 
    let val i = nextID () 
	val _ = countNew ()
    in ref (i, dummyInvalidator)
    end

   fun toString (ref (i,_)) = Int.toString i

   fun listToString l = 
     case l of
       nil => "nil"
     | (ref (i,_))::t => Int.toString (i) ^ ": " ^ listToString t

   fun allToString () = listToString (!time)

  fun init () = 
    let
      val _ = ID := 0
      val first = new ()
      val _ = time := [first]
    in
      first
    end

  fun find e l =  
    case List.find (fn x => eq (x,e)) l of
      NONE => false
    | SOME _ => true

  fun isLive ( t as ref (i,_)) = 
    if i < 0 then 
      false
    else
      find t (!time)

  fun isSplicedOut (ref (i,_)) =  i < 0

  fun assert (e) = if (isLive e) then ()
                   else 
                     let
                       val _ = print ("Asserting " ^ toString e ^ "\n")
                       val _ = print ("Time = " ^ allToString () ^ "\n")
                     in
                       raise assertFailed
                     end

   fun verify () = 
     let
       fun all l = 
         case l of
           nil => true
        | (ref (i,_))::t => 
           if (i < 0) then
             raise verifyFailed
           else
             all t
     in
       all (!time)
     end

  fun size _ = 
    List.length (!time)

  fun splitAndDelete l e = 
    case l of 
      nil =>  (nil,nil)
    | h::t => 
        if (eq(h,e)) then 
          (nil,t)
        else
           let val (la,lb) = (splitAndDelete t e) 
           in (h::la,lb)
           end

  fun split l e = 
    case l of 
      nil =>  (nil,nil)
    | h::t => 
        if (eq(h,e)) then 
          ([e],t)
        else
           let val (la,lb) = (split t e) 
           in (h::la,lb)
           end

   fun add e = 
     let
       val _ = assert e
     in
       if not (find e (!time)) then
         raise badInput
       else
       let 
           val _ = countInsert ()
           val (la,lb) = split (!time) e 
           val n = new ()
       in (time := List.@(la,n::lb) ; n)
       end
     end
  
   fun addNew (e,n) = 
     let
       val _ = assert e
     in
       if not (find e (!time)) then
         raise badInput
       else
       let 
           val _ = countInsert ()
           val (la,lb) = split (!time) e 
       in (time := List.@(la,n::lb) ; n)
       end
     end

   fun getNext e = 
     let val (_,l) = split (!time) e
     in
       case l of
         nil => NONE
       | h::_ => SOME h
     end
   
   fun next e = 
     let val (_,l) = split (!time) e
     in
       case l of
         nil => let val h::t = !time in h end
       | h::_ => h
     end

  fun compare (ea,eb) = 
    if (eq(ea,eb)) then
      EQUAL 
    else
      let
        val (la,lb) = split (!time) ea
      in
        if (find eb lb) then
          LESS
        else if (find eb la) then
          GREATER
        else
	  raise badInput
      end

  fun deleteStamp (t as ref (i,inv)) =
      (inv (); t := (~1, dummyInvalidator))

  fun spliceOut (start,stop) = 
    let
      fun markDeleted l = 
        case l of 
          nil => nil
        | (h as ref (i, _))::t => 
	  (countDelete (); 
	   deleteStamp h;
	   markDeleted t)

     val _ = dPrint ("Splicing out: start = " ^ toString start  ^ " stop = " ^ toString stop ^ "\n")
     val _ = dPrint ("Time List =  " ^ allToString () ^ "\n") 
     val _ = assert (start)
     val _ = assert (stop)
     val _ = if not (find  start (!time) andalso find stop (!time)) then
               raise badInput
             else   
               ()

    in
      case compare (start,stop) of 
        EQUAL => 0
      | GREATER =>  raise badInput 
      | LESS => 
        let
          val (la,lb) = split (!time) start
          val (lba,lbb) = splitAndDelete lb stop
          val _ = markDeleted lba
          val _ = assert (start)
          val _ = assert (stop)
        in
          (time := List.@(la,stop::lbb); 
           List.length (lba))
        end
    end

  fun setInv (t,g) =   
    let val (i,f) = !t in t := (i,g) end

  fun resetInv t = 
    let val (i,f) = !t in t := (i,dummyInvalidator) end

  fun addToInv (t,g) = 
    let val (i,f) = !t in t := (i,fn () => (f ();  g ())) end
    
  fun getInv t = 
    let val (i,f) = !t in f end

  fun statsToString () =
    let
      val {deleted=d,new=n,inserted=i} = !counters
              
    in
       "Deleted = " ^ Int.toString d ^ " " ^
       "New = " ^ Int.toString n  ^ " " ^
       "Inserted = " ^ Int.toString i  ^ " " ^
       "Size = " ^ Int.toString (size ()) ^ " " ^
       "ID = " ^ Int.toString (!ID) 

    end

 
  fun app f start =  List.app f (!time)
end
