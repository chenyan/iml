(**********************************************************************
 ** TimeStamps64.sml
 **
 ** Copyright 2005 Umut A. Acar
 **
 ** Written by Umut A. Acar, Guy Blelloch, and Kanat Tangwongsan.
 **
 ** This program is distributed under the terms of the GNU General
 ** Public License as published by the Free Software Foundation.  See
 ** the LICENSE file for the terms and conditions.
 **
 ***************
 ** Description: A 64-bit implementation of an order-maintenance data
 ** structure
 **********************************************************************)

structure TimeStamps : ORDERED_LIST = 
struct 

  open Word64

  exception outOfTimeStamps
  exception AssertionFail
  exception InternalError

  val w_zero = fromInt(0)
  val w_one = fromInt(1)
  val w_two = fromInt(2)

(****************************************************************)
(****************************************************************)
(*    TOP LEVEL  (see bottom level below)                       *)
(****************************************************************)
(****************************************************************)

  datatype 'a TopLevelNode =
      NULL
    | TN of (('a TopLevelNode ref) (* prev *) * 
	     (word ref)            (* id   *) *
	     ('a ref)              (* data *) *
	     ('a TopLevelNode ref) (* next *) )
      
  exception badTop

(* Constant for internal use. 
   Smaller makes it more expensive but gives more stamps *)

  val T = 1.41421 

  fun wordToReal w = Real.fromLargeInt((toLargeInt w))

  fun TNTopID (TN(_,ref id,_,_)) = id

  fun TNTopIDRef (TN(_,id,_,_)) = id
  fun TNNextRef (TN(_,_,_,nxref)) = nxref

  fun TNPrevRef (TN(pvref,_,_,_)) = pvref

  fun TNData (TN(_,_,ref data,_)) = data

  local  

    val maxVal = fromInt(~1)

    fun findMargin(mask,tau:real,lo,hi,bt,n_sofar) =
	let
	  val lo_num = andb ((notb mask), bt)
	  val hi_num = orb (lo_num,mask)

	  fun extendLeft cur c =
	      let 
		  val TN(ref pv,ref myid,_,_) = cur
		  val prevID = TNTopID(pv) 
	      in
		  if ((prevID>=lo_num)
		      andalso (prevID<=myid)) then
		      extendLeft pv (c+w_one)
		  else
		      (cur,c)
	      end

	  fun extendRight cur c =
	      let 
		  val TN(_,ref myid,_,ref nx) = cur
		  val nxID = TNTopID(nx) 
	      in
		  if ((nxID<=hi_num)
		      andalso (nxID>=myid)) then
		      extendRight nx (c+w_one)
		  else
		      (cur,c)
	      end

	  val (lo', temp_count) = extendLeft lo n_sofar
	  val (hi',count_total) = extendRight hi temp_count
	  val gap_size' = mask + w_one
	  val density = (wordToReal(count_total))/(wordToReal(gap_size'))
	in
	  (if (Real.<(density, tau)) then
	     (lo',hi',count_total,gap_size',lo_num,hi_num)
	   else
	     let
	       val newmask = mask*w_two + w_one
	     in
  	       if (newmask = maxVal) then raise outOfTimeStamps
	       else
		   findMargin(newmask, tau/T, lo', hi', 
			      bt, count_total)
	     end
	     )
	end
      
    fun scanUpdate (cur,last,num,incr) =
	(
	 (TNTopIDRef(cur)):= num;
	 if (cur=last) then ()
	 else scanUpdate (!(TNNextRef(cur)), last, num+incr, incr)
	)
    
    fun issueTopLevelNumber p = 
	let
	  val left = TNTopID(!(TNPrevRef(p)))
	  val t' = TNTopID(!(TNNextRef(p)))
	  val right = if (t'<=left) then left+w_two else t'
	in
	  if ((left+w_one) = right) then
	    (let 
	       val(lo,hi,total,gap_size,start_tag,_) =
		 findMargin(w_one,1.0/T,p,p,TNTopID(p), w_one)
	       val incr = gap_size div total
	     in
	       scanUpdate (lo,hi,start_tag,incr)
	     end)
	  else
	    ((TNTopIDRef(p)) := ((left+right) div w_two))
	end

  in

    fun addTop(node, value) = 
      let
	val TN(prevRef,ref pID,_,nextRef as ref next) = node
	val newNode = TN(ref(node), ref(pID), ref(value), ref(next))
        val TN(nextPrevRef,_,_,_) = next
	val _ = nextRef := newNode
	val _ = nextPrevRef := newNode
	val _ = issueTopLevelNumber newNode
      in
	newNode
      end

    fun deleteTop node =
      ((TNPrevRef(!(TNNextRef node))) := !(TNPrevRef node);
       (TNNextRef(!(TNPrevRef node))) := !(TNNextRef node))

  end

(****************************************************************)
(****************************************************************)
(*    BOTTOM LEVEL                                              *)
(****************************************************************)
(****************************************************************)

  val maxVal = (fromInt(~1) div 0w2)+0w1;
  val lSize = fromInt 64
  val gapSize = maxVal div lSize
  val endV = maxVal - gapSize

  exception badNode

  exception badNode10
  exception badNode01
  exception badNode11
  
  type invalidater = (unit ->unit) 
  val dummyInvalidater = fn () => ()

  datatype ChildNodeV = 
    SLACK 
  | CN of (ChildNodeV ref TopLevelNode * (* parent *)
	   word *                   (* ID *)
	   invalidater *            (* Invalidater *)
	   ChildNodeV ref)           (* next *)

  type ChildNode = ChildNodeV ref
  type t = ChildNode

  val slackRef = ref SLACK
  val invalid = ref SLACK
  val special = ref(CN (NULL, w_one, dummyInvalidater, slackRef))

  fun isSpecial n = (n = special)
  fun isSplicedOut (node : t) = 
      case (!node) of
	  SLACK => true
	| _ => false

 (**********************************************************************
  ** Mainline
  **********************************************************************)

  fun nodeToString node =
    case !node of
      SLACK => ("[dead]")

    | CN(parent, id, _, next) =>
      let
        val parentString = 
	    case parent of
		NULL => raise badNode
	      | _ => toString (TNTopID parent)
	val nextString =
	    case !next of
		SLACK => "end"
	      | CN(_,nextID,_,_) => toString nextID
    in
      ("[id=" ^ (toString id) ^
       " [p=" ^ parentString ^ "]" ^
       " [n=" ^ nextString ^"]]")
    end


  fun add node =
      let 
	  fun balanceList (child, parent) =
	      let
		  fun processBlock (node, num) =
		      case !node of
			  SLACK => node
			| CN(_,_,inv, next) =>
			  (if (num>=endV) then 
			       (node := CN(parent, num, inv,slackRef); 
				next)
			   else 
			       (node := CN(parent, num, inv, next);
				processBlock (next, num+gapSize)))

		  val nextBlock = processBlock(child,0w0)
	      in
		  case !nextBlock of
		      SLACK => ()
		    | _ => balanceList(nextBlock,
				       addTop(parent, nextBlock))
	      end

	  val CN(parent, ID, inv, next) = !node
          val nextID = case !next of
		           CN(_, nextID,_, _) => nextID
			 | SLACK => maxVal
          val newID = Word64.>>(ID + nextID, 0w1)
	  val newNode = ref(CN(parent, newID, dummyInvalidater, next))
      in
	  (node := CN(parent, ID, inv, newNode);
	   (if (newID = ID) then 
		balanceList(TNData parent, parent)
	    else ());
	   newNode)
      end

  fun compare (node1, node2) =
      let
      in
	  case (!node1,!node2) of
	      (CN(TN(_,ref p1, _, _), ID1, _, _), CN(TN (_, ref p2, _, _), ID2,_, _)) =>
	      let
	      in
		case Word64.compare(p1, p2) of
  		  EQUAL => Word64.compare(ID1, ID2)
  		| x => x 
	      end
	    | (SLACK,_) => LESS
	    | _ => GREATER
      end

  fun ordered (node1, node2, node3) =
    let
    in
	case (!node1,!node2,!node3) of
	    (CN(TN(_, ref p1, _, _), ID1, _, _), CN(TN(_, ref p2, _, _), ID2, _, _), CN(TN(_,ref p3,_,_), ID3, _, _)) =>
              ((Word64.compare(p1, p2) = LESS) orelse (Word64.compare(p1, p2) = EQUAL andalso  Word64.compare (ID1,ID2) = LESS)) andalso
              ((Word64.compare(p2, p3) = LESS) orelse (Word64.compare(p2, p3) = EQUAL andalso  Word64.compare (ID2,ID3) = LESS))
    end

  fun getNext node =
    let val n = let val CN(parent, myid, _, next) =  !node 
		 in
                    case !next of
			SLACK => SOME(TNData(!(TNNextRef parent)))
                      | _ => SOME(next)
		 end
    in case n of
        NONE => NONE
      | SOME(next) => 
          case compare (node,next) of
            LESS => n
          | _ => NONE
    end

  fun next node =
    let val CN(parent, myid, _, next)  = !node  in
	case !next of
	  SLACK => TNData(!(TNNextRef parent))
	| _ => next
    end

  fun spliceOut (start,stop) =
    let
      fun deleteRange' (next,parent) : ChildNode =
        if (next=stop) then stop
	else

	  case !next of
	    CN(_, _, inv, nextNext) =>
	        (
		 inv (); 
			next := SLACK; 
	         deleteRange'(nextNext,parent))
	  | SLACK =>
	     let
	       val nextParent = !(TNNextRef parent)
	       val TN(_,_,nextChildRef,_) = nextParent
	       val newNext = deleteRange'(!nextChildRef,nextParent)
	     in 
    	       ((case !newNext of
		   SLACK => deleteTop(nextParent)
		 | _ => nextChildRef := newNext);
		slackRef)
	     end
       val CN(parent, ID, inv,  next) = !start
    in
        case (compare(start,stop)) of
	    LESS => (start := CN(parent,ID, inv, deleteRange'(next,parent)); 0)
	  | _ => 0
    end

  fun spliceOut'(start, stop) =  spliceOut(start,next(stop)) 

  fun getFirst node = 
    let
      fun fstP p =
	let
	  val TN(ref prev, ref ID, ref child,_) = p
	  val TN(_ , ref pID,_,_) = prev
	in
	  if pID >= ID then child
	  else fstP prev
	end
      val CN(p,_,_,_) = !node
    in
	fstP p
    end

  fun size node = 
    let
      val first = getFirst node
      fun ct(n) =
	let
	  val next = next(n)
	in
	  if (next=first) then 1
	  else 
	    case compare(n,next) handle e => raise e of
              LESS => Int.+(1,ct(next))
	    | _ => (raise AssertionFail)
	end
    in
      ct first
    end

  fun resetInv node = ()

  fun setInv (node, g : invalidater) =
     let
	 val CN(p, ID, f, next) = !node
     in
	 (node := CN(p, ID, g, next))
     end

  fun addToInv (node, g : invalidater) =
      let val CN(p, ID, f, next) = !node 
      in
	  (node := CN(p, ID, fn () => (f (); g ()), next)) 
      end

  fun getInv node = 
      let val CN(_ ,_ , f, _) = !node in f end

  fun moveInv (from, to) = raise InternalError

  fun toString n = nodeToString n
  fun assert n = if isSplicedOut n then raise InternalError else ()

  fun app f start = 
      let
	  fun loop t  = 
	      if compare (t,start)=EQUAL then
		  ()
	      else
		  let val _ = f t 
		  in loop (next t)
		  end
	  val _ = f start
      in loop (next start)
      end
  
  fun init () =
    let val newNode = ref SLACK
        val TNprevRef = ref(NULL)
        val TNnextRef = ref(NULL)
        val newParent = TN(TNprevRef, ref(w_zero), ref(newNode), TNnextRef)
        val _ = TNprevRef := newParent;
        val _ = TNnextRef := newParent;
        val _ = newNode := CN(newParent, w_zero, dummyInvalidater, slackRef)
     in newNode
     end
       
end
