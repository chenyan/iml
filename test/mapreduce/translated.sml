structure MapReduce =
struct

structure C = Comb
structure M = Meta

type 'a modref = 'a Modref.modref

structure Hash = HashFamily(val M = 2)

val modref = C.modref
val read = C.read
val new = C.fromVal

datatype sbool = Strue
               | Sfalse
datatype cbool = Ctrue
               | Cfalse
datatype 'a block = Nil
                  | Cons of ('a * 'a block modref)

val rec less = fn x_18747 =>
(fn ((x , y)) =>
 (fn true => Strue
  | false => Sfalse) ((Int.< (x , y)))) ((x_18747))
type key = string
type value = int
type pair = (key * value)

infix ==> val op ==> = C.read

fun eqSC (a,b) =
  case (a,b) of
    (Nil,Nil) => true
   | (Cons(ha,ta), Cons(hb,tb)) => Box.eq(ha,hb) andalso (ta=tb)
   | _ => false

fun eqCC (a,b) =
  case (a,b) of
    (Nil,Nil) => true
   | (Cons(ha,ta), Cons(hb,tb)) => ha=hb andalso (ta=tb)
   | _ => false

val rec mapSC = fn x_18749 =>
fn x_18750 =>
let 
    val write = C.write' MLton.eq 
    val lift = C.mkLiftCC_one ((fn (a,b) => a=b), MLton.eq)
in
  (fn (f , l) =>
   read ((l) , (fn Nil => (write Nil)
                | (Cons (h , t)) => lift ([Box.indexOf h], t) (fn args =>
                  (write (Cons ((f h) , (modref (read(args, fn t => (mapSC f) t)))))))))) ((x_18749 , x_18750))
end
val rec lengthLessThan = fn x_18751 =>
fn x_18752 => 
let val write = C.write' MLton.eq in
  (fn (n , l) =>
   (fn Strue => (write Cfalse)
    | Sfalse =>
      (read ((l) , (fn Nil => (write Ctrue)
                    | (Cons (h , t)) =>
                      ((lengthLessThan ((Int.- (n , 1)))) t))))) ((less (n , 1)))) ((x_18751 , x_18752))
end
val rec reduce = fn x_18757 =>
fn x_18758 =>
  fn x_18759 =>
let 
  val write = C.write' MLton.eq 
  fun hash h = Hash.hashFun () (Box.indexOf h)
in
    (fn (id , binOp , l) =>
     
         let
           val rec halfList = fn x_18755 =>
           (fn (l) =>
            
                let
                  val lift = C.mkLiftCC_one (fn(a,b) => a=b, eqSC)
                  val rec half = fn x_18754 =>
                  (fn (l) =>
                   
                       let
                         val rec sumRun = fn x_18753 =>
                         (fn ((v , l)) =>
                          read ((l) , (fn Nil =>
                                       (C.write' MLton.eq (v , (modref (write Nil))))
                                       | (Cons (h , t)) =>
                                         
                                             let
                                               val vh = binOp (v , h)
                                             in
                                               (fn true => (C.write' MLton.eq (vh , t))
                                                | false =>
                                                  (sumRun (vh , t))) ((( ((hash (h)) = 0))))
                                             end))) ((x_18753))
                       in
                         read ((l) , (fn Nil => (write Nil)
                                      | (Cons (h , t)) => lift ([Box.indexOf h],t) (fn arg =>
                                        
                                            let
                                              val tmp = modref (arg ==> (fn t => (sumRun (h , t))))
                                              val tail = modref (read (tmp, fn (_,rest) => (half rest)))
                                            in
                                              read (tmp, fn(v,_) => (write (Cons (v , tail))))
                                            end)))
                       end) ((x_18754))
                in
                  (half l)
                end) ((x_18755));
           val rec comb = fn x_18756 =>
           (fn (l) =>
            
                let
                  val p = modref (((lengthLessThan 2) l))
                in
                  read ((p) , (fn Ctrue =>
                               (read ((l) , (fn Nil => (C.write' MLton.eq id)
                                             | (Cons (h , _)) => C.write' MLton.eq (binOp(id,h)))))
                               | Cfalse =>
                                 
                                     let
                                       val r = modref ((halfList l))
                                     in
                                       (comb r)
                                     end))
                end) ((x_18756))
         in
           (comb l)
         end) ((x_18757 , x_18758 , x_18759))
end

val write = C.write'

val rec split = fn x_18762 =>
fn x_18763 =>
  let val lift = C.mkLiftCC_one (fn (a,b) => a=b, MLton.eq) in
  (fn (f , l) =>
   read ((l) , (fn Nil =>
                (write MLton.eq ((modref (write MLton.eq Nil)) , (modref (write MLton.eq Nil))))
                | (Cons (h , t)) => lift ([Box.indexOf h],t) (fn arg =>
                  
                      let
                        val x = f h
                        val tmp = modref (arg ==> (fn t => ((split f) t)))
                      in
                        case x of
                          true =>
                          let
                            val a' = modref (read(tmp, fn (a,_) => (write MLton.eq (Cons (h , a)))))
                          in
                            read(tmp, fn(_,b) => write MLton.eq (a' , b))
                          end
                        | false =>
                          let
                            val b' = modref (read(tmp, fn (_,b) => (write MLton.eq (Cons (h , b)))))
                          in
                            read(tmp, fn(a,_) => write MLton.eq (a , b'))
                          end
                      end)))) ((x_18762 , x_18763))
  end

val rec merge = fn x_18764 =>
let
  val lift = C.mkLift1_one MLton.eq
in
(fn ((a , b)) => 
  read (a, fn a' => read (b, fn b' => 
   case (a',b') of 
     (Nil , _) => write MLton.eq b'
   | (_ , Nil) => write MLton.eq a'
   | ((Cons (ha , ta)) , (Cons (hb , tb))) =>
     let
       val k1 = #1(Box.valueOf ha)
       val k2 = #1(Box.valueOf hb)
     in
      (case String.<(k1,k2) of
         true => lift ([Box.indexOf hb],(a,tb)) (fn args =>
         (write MLton.eq (Cons (hb , (modref (args ==> (fn (a,tb) => merge (a , tb))))))))
       | false => lift ([Box.indexOf ha],(ta,b)) (fn args =>
         (write MLton.eq (Cons (ha , (modref (args ==> (fn (ta,b) => merge (ta , b))))))))) 
     end))) ((x_18764))
end

val rec sort = fn x_18766 =>
(fn (l) =>
     let
       fun hash h = Hash.hashFun () (Box.indexOf h)
       val rec sort' = fn x_18765 =>
       (fn (l) =>
        read (((modref ((lengthLessThan 2) l))) , (fn Ctrue =>
                                                   (read (l , fn x_20205 =>
                                                          write MLton.eq x_20205))
                                                   | Cfalse =>
                                                     
                                                         let
                                                           val tmp = modref (((split (fn h => hash(h)=0) l)))
                                                           val even_s = modref (read (tmp, (fn(even,_) => (sort' (even)))))
                                                           val odd_s = modref (read (tmp, (fn(_,odd) => (sort' (odd)))))
                                                         in
                                                           (merge (even_s , odd_s))
                                                         end))) ((x_18765))
     in
       (sort' l)
     end) ((x_18766))
val rec groupby = fn x_18770 =>
(fn (l) =>
     let
       val l = modref ((sort l))
       val lift = C.mkLiftCC_one (MLton.eq, MLton.eq)
       val rec take = fn x_18767 =>
       fn x_18768 =>
         fn x_18769 =>
           (fn (key , l , acc) =>
            read ((l) , (fn Nil =>
                         (write MLton.eq (Cons (Box.new acc , (modref (write MLton.eq Nil)))))
                         | (Cons (h , t)) =>
                           let
                             val (cur_key,v) = Box.valueOf h
                           in
                           (fn true => lift ([Box.indexOf h], (t,acc)) (fn args =>
                            (args ==> (fn (t,acc) => ((take key) t) ((modref (write MLton.eq (Cons (h , acc))))))))
                            | false => lift ([Box.indexOf h], (t,acc)) (fn args =>
                              (args ==> (fn (t,acc) => write MLton.eq (Cons (Box.new acc , (modref (((take cur_key) t) ((modref (write MLton.eq (Cons (h , (modref (write MLton.eq Nil))))))))))))))) ((( (key = cur_key)))) end))) ((x_18767 , x_18768 , x_18769))
     in
       read(l, fn Cons(h,_) => (((take (#1(Box.valueOf h)) l) (modref (write MLton.eq Nil)))))
     end) ((x_18770))

fun mapreduce mapper reducer input = 
  let
    val (id,binop) = reducer
    val pairs : pair Box.t block modref = modref (mapSC mapper input)
    val merged = modref (groupby pairs)
    val result : pair Box.t modref block modref = modref (mapSC (fn block => modref (reduce id binop (Box.valueOf block))) merged)
  in
    result
  end

end
