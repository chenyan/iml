
open MapReduce

structure C = Comb
structure M = Meta

val deref = M.deref
val new = C.fromVal
val change = M.change' MLton.eq

val mapper = fn s => Box.new (Box.valueOf s,1)
val reducer = (Box.new ("",0), 
            fn (acc,cur) => let
                val (_,acc) = Box.valueOf acc
                val (key,v) = Box.valueOf cur
              in Box.new (key,acc+v) end)
val wordcount : string Box.t block modref -> pair Box.t modref block modref = mapreduce mapper reducer

fun printresult l =
  case deref l of
    Nil => print "\n"
  | Cons(h,t) => 
    let
      val (key,v) = Box.valueOf (deref h)
    in
      (print ("(" ^ key ^ "," ^ (Int.toString v) ^ ")");
       printresult t)
    end

fun toBlock l acc =
  case l of
    nil => acc
  | h::t => new (Cons(Box.new h, toBlock t acc))

fun removeModref l =
  case deref l of
     Nil =>  Normal.Nil
   | Cons(h,t) => Normal.Cons(h, removeModref t)

fun neq (nl,l) =
  case (nl, deref l) of
     (Normal.Nil, Nil) => true
   | (Normal.Cons(h1,t1), Cons(h2,t2)) => 
     let
       val (k1,v1) = Box.valueOf h1
       val (k2,v2) = Box.valueOf (deref h2)
     in
       k1=k2 andalso v1=v2 andalso neq (t1,t2)
     end
   | _ => false

fun readfile strm = 
let
  val _ = Meta.init () 

  fun getLine () = 
      case TextIO.inputLine strm of
          NONE => NONE
        | SOME line => SOME (String.tokens Char.isSpace line)

  fun getInitialInput nline acc =
      if nline>1 then acc
      else let
          val words = getLine()
      in case words of
             NONE => acc
           | SOME words => getInitialInput (nline+1) (toBlock words acc)
      end

  val words = getInitialInput 1 (new Nil)
  val result = wordcount words
  val _ = printresult result

  fun insertWords () =
      let
          val new_words = getLine()
          fun addwords l =
              case l of
                  nil => ()
                | h::t => 
                  let
                    val list = new (deref words)
                    val _ = change words (Cons(Box.new h,list))
                  in
                    addwords t
                  end
          fun verifier l = Normal.mapreduce mapper reducer l
      in
          case new_words of
              NONE => ()
            | SOME new_words => 
              (addwords new_words;
               Meta.propagate();
               printresult result;
               (*(if neq (verifier (removeModref words), result)
               then print "correct\n" else print "wrong!\n");*)
               insertWords())
      end
in
    insertWords()
end

val _ =
  (case CommandLine.arguments () of
    file::nil =>
      let
        val strm = TextIO.openIn file
      in
         readfile strm
         handle x => (TextIO.closeIn strm; raise x);
         TextIO.closeIn strm;
         true
       end
   | _ => (print "Usage = .... < file name >\n"  ; false))
  handle IO.Io {name, function, cause} => 
       (print (name^","^exnMessage cause^"\n");
        false)




