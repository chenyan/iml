(**********************************************************************
 ** Controls.sml
 **
 ** Copyright 2005 Umut A. Acar
 **
 ** Written by Umut A. Acar
 **
 ** This program is distributed under the terms of the GNU General
 ** Public License as published by the Free Software Foundation.  See
 ** the LICENSE file for the terms and conditions.
 **
 ***************
 ** Description: Global controls for the library
 **********************************************************************)

structure Controls = 
struct
  
  val SPACE_PROFILING = false 
  fun profiling () = SPACE_PROFILING
end
