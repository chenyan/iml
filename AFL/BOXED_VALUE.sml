(**********************************************************************
 ** BOXED_VALUE.sml
 **
 ** Copyright 2005 Umut A. Acar
 **
 ** Written by Umut A. Acar
 **
 ** This program is distributed under the terms of the GNU General
 ** Public License as published by the Free Software Foundation.  See
 ** the LICENSE file for the terms and conditions.
 **
 ***************
 ** Description: The signature for boxed values.
 **********************************************************************)

signature BOXED_VALUE = 
  sig

    type index=int
    type 'a t

    val init: unit -> unit    

    val new: 'a -> 'a t	
    val eq: 'a t * 'a t -> bool

    val newIndex: unit -> int
    val mkBox: int*'a -> 'a t

    val fromInt: int -> int t
    val fromOption: 'a t option -> 'a t option t

    val valueOf: 'a t -> 'a
    val indexOf: 'a t -> index

    val toString: 'a t -> string

  end
