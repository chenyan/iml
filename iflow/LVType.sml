structure LVType =
struct
  open GrammarProgram
  open GrammarModule
  open GrammarCore
       
  open StaticObjectsCore
  open PrettyPrint
  open Error

  infixr ^^ ^/^

  open LExp
  open Constraint
  open Solver

  fun disp f = (PrettyPrint.output (TextIO.stdOut, f ^/^ text "\n", 79);
                TextIO.flushOut TextIO.stdOut)

  val error = fn f => fn str => (PrettyPrint.output (TextIO.stdOut, f, 79);
                              TextIO.flushOut TextIO.stdOut;
                              error(Source.nowhere(), "\n"^str))
  val errorTy = fn tau => error (PPType.ppType (tau))
  val errorLv = fn lv => error ((PrettyPrint.text o Level.toString) (!lv))


  fun getReferTy I = case peekRefer I of
                       SOME I => getType(I)
                     | NONE => (Error.error(I,"no refer type"); getType(I))

  fun peekLv tau =
    case !tau of
      RowType (_,_,lv) => SOME lv
    | FunType (_,_,_,lv) => SOME lv
    | ConsType (_,_,lv) => SOME lv
    | _ => NONE
  fun getLv tau = case peekLv tau of
      SOME lv => lv 
    | NONE => ref Level.Unknown


  fun unifyLv f (lv1,lv2) = 
   case (!lv1,!lv2) of
     (* eliminate known level, instantiate unknown *) 
     (Level.Undetermined S,_) => (lv1 := Level.Stable; unifyLv f (lv1,lv2))
   | (_, Level.Undetermined S) => (lv2 := Level.Stable; unifyLv f (lv1,lv2))
   | (Level.Undetermined C,_) => (lv1 := Level.Changeable; unifyLv f (lv1,lv2))
   | (_, Level.Undetermined C) => (lv2 := Level.Changeable; unifyLv f (lv1,lv2))
   | (Level.Unknown, _) => (lv1 := Level.Undetermined (Var (gensym())); unifyLv f (lv1,lv2))
   | (_, Level.Unknown) => (lv2 := Level.Undetermined (Var (gensym())); unifyLv f (lv1,lv2))
     (* unify *)
   | (Level.Undetermined (Var v1), Level.Undetermined (Var v2)) => f (Var v1, Var v2)
   | (Level.Undetermined (Var v1), Level.Stable) => f (Var v1, S)
   | (Level.Undetermined (Var v1), Level.Changeable) => f (Var v1, C)
   | (Level.Stable, Level.Undetermined (Var v2)) => f (S, Var v2)
   | (Level.Changeable, Level.Undetermined (Var v2)) => f (C, Var v2)
   | (Level.Stable, Level.Changeable) => f (S, C)
   | (l1,l2) => if (l1 = l2) then () else raise Unify
               (*error (text (Level.toString (!lv1)) ^/^ text (Level.toString (!lv2))) "unifyLv"*)

  fun unifyTy f (ty1,ty2) = case (!ty1,!ty2) of
    (Undetermined _, _) => ()
  | (_, Undetermined _) => ()
  | (Overloaded _, _) => ()
  | (_, Overloaded _) => ()
  | (TyVar _, _) => ()
  | (_, TyVar _) => ()
  | (FunType(tau11,tau12,mode1,lv1), FunType(tau21,tau22,mode2,lv2)) => 
       (unifyTy f (tau21,tau11);
        unifyTy f (tau12,tau22);
        unifyLv f (lv1,lv2);
        unifyLv f (mode1,mode2);
        unifyLv f (mode2,mode1))
  | (ConsType(taus1,t1,lv1), ConsType(taus2,t2,lv2)) =>
       (ListPair.appEq (unifyTy f) (taus1,taus2);
        unifyLv f (lv1,lv2))
  | (RowType(rho1,r1,lv1), RowType(rho2,r2,lv2)) =>
       (LabMap.appi (fn (lab,ty1) => case LabMap.find(rho2, lab) of
                                       SOME ty2 => unifyTy f (ty1,ty2)
                                     | NONE => errorTy ty2 "record type missing")
                    rho1;
        unifyLv f (lv1,lv2))
  | _ => raise Unify (*error (PPType.ppType ty1 ^/^ PPType.ppType ty2) "unifyTy"*)

    
  fun ltTy f (lv:Level.t ref, ty) = case !ty of
    FunType(_,_,_,lv') => unifyLv f (lv,lv')
  | ConsType(taus,_,lv') => unifyLv f (lv,lv')
  | RowType(rho,_,lv') => unifyLv f (lv,lv')
  | _ => ()

  fun substLv f lv = case !lv of
    Level.Undetermined S => lv := Level.Stable
  | Level.Undetermined C => lv := Level.Changeable
  | Level.Undetermined lexp => lv := Level.Undetermined (f lexp)
  | Level.Unknown => lv := Level.Undetermined (Var (gensym()))
  | _ => ()


  fun substTy f ty = case !ty of
    FunType(tau1,tau2,mode,lv) => 
      (substTy f tau1; substTy f tau2;
       substLv f mode; substLv f lv)
  | ConsType(taus,_,lv) => (List.app (substTy f) taus; substLv f lv)
  | RowType(rho,_,lv) => (LabMap.app (substTy f) rho; substLv f lv)
  | _ => ()
             
  fun copyTy ty =
    let
      val copy_ty =
          case !ty of
            RowType (lab,r,lv) => RowType (LabMap.map copyTy lab,r,ref (!lv))
          | FunType (a,b,mode,lv) => FunType (copyTy a,copyTy b,ref (!mode),ref(!lv))
          | ConsType (tys,name,lv) => ConsType (map copyTy tys,name,ref(!lv))
          | Determined ty => !(copyTy ty)
          | t => t  (* Undermined are generalized types *)
      val ty = ref copy_ty
    in
      ty
    end

  fun instanceTy c f (tau,sigma,(vars,cons)) = 
    let
      val (alphas,general) = sigma
      val tmu = ref TyVarMap.empty
      val new_vars = Solver.copy c vars
      val mu = ListPair.zipEq (vars,new_vars)
(*
      val _ = disp(text "generalized vars:" ^/^ text (lexpsToString (List.map Var vars)))
      val _ = disp(text "general before:" ^/^ PPType.ppTypeScheme (#1 sigma,general))
*)
      val _ = substTy (clone mu) general
(*
      val _ = disp(text "general after:" ^/^ PPType.ppTypeScheme (#1 sigma, general))
      val _ = disp(text "instance before:" ^/^ PPType.ppType tau)
*)
      fun instance (ty1,ty2) =
        case (!ty1, !ty2) of
          (t, TyVar alpha) => 
            (case TyVarMap.find (!tmu, alpha) of
               NONE => (tmu := TyVarMap.insert(!tmu, alpha, ty1))
             | SOME ty => ())
        | (FunType(tau11,tau12,mode1,lv1), FunType(tau21,tau22,mode2,lv2)) =>
            (instance (tau21,tau11);
             instance (tau12,tau22);
             unifyLv f (lv1,lv2);
             unifyLv f (mode1,mode2);
             unifyLv f (mode2,mode1))
        | (ConsType(taus1,t1,lv1), ConsType(taus2,t2,lv2)) =>
            (ListPair.appEq instance (taus1,taus2);
             unifyLv f (lv1,lv2))
        | (RowType(rho1,r1,lv1), RowType(rho2,r2,lv2)) =>
            (LabMap.appi (fn (lab,ty1) => case LabMap.find(rho2,lab) of
                                            SOME ty2 => instance (ty1,ty2)
                                          | NONE => errorTy ty2 "record type missing")
                         rho1;
             unifyLv f (lv1,lv2))
        | (t1, t2) => ()
      val _ = instance(tau,general)
      val tau' = Type.substitute (!tmu) general
    in
      unifyTy f (tau,tau')
      (*; disp(text "instance after:" ^/^ PPType.ppType tau)*)
    end

    
  fun getASTTy ty =
    case ty of
      VARTy(I,tyvar) => Type.fromTyVar tyvar
    | RECORDTy(I,tyrow_opt,lv) => 
      let
        val rho = case tyrow_opt of
                    NONE => Type.emptyRow
                  | SOME tyrow => getTyRow tyrow
        val (_,_,level) = rho
        val _ = level := lv
        val tau = Type.fromRowType rho
      in tau end
    | CONTy(I, tyseq, longtycon, lv) => 
      let
        val tyname = Type.tyname (getType I)
        val Tyseq(I',tys) = tyseq
        val taus = List.map getASTTy tys
      in Type.fromConsType (taus, tyname, ref lv) end
    | ARROWTy(I,ty,ty',mode,lv) => 
      let
        val tau1 = getASTTy ty
        val tau2 = getASTTy ty'
      in Type.fromFunType(tau1,tau2,ref mode,ref lv) end
    | PARTy(I,ty) => getASTTy ty
  and getTyRow (TyRow(I,lab,ty,tyrow_opt)) =
      let
        val tau = getASTTy ty
        val rho = case tyrow_opt of
                    NONE => Type.emptyRow
                  | SOME tyrow => getTyRow tyrow
      in Type.insertRow(rho,lab,tau) end
      
end
