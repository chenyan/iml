(**********************************************************************
 ** IModref.sml
 **
 ** Copyright 2005 Umut A. Acar
 **
 ** Written by Umut A. Acar, Guy Blelloch, Robert Harper
 **
 ** This program is distributed under the terms of the GNU General
 ** Public License as published by the Free Software Foundation.  See
 ** the LICENSE file for the terms and conditions.
 **
 ***************
 ** Description: This implementation is described in Acar's thesis
 ** "Self-Adjusting Computation", (Carnegie Mellon University
 ** Department of Computer Science, Technical Report. CMU-CS-05-129).
 **********************************************************************)

structure IModref : I_MODIFIABLE=
struct 

val DEBUG = false

type time = TimeStamps.t
type changeable = unit 
 

exception UnsetMod 
exception AssertionFail
exception DummyRead
exception BadCode
exception InternalError
exception UnsafeUseOfReadNoTime
exception Impossible
exception TROUBLE

(**********************************************************************
 ** Some Flags
 **********************************************************************)
 val PROPAGATION_IN_PROGRESS = ref false

(**********************************************************************
 ** Time Stamps
 **********************************************************************)
val base = ref (TimeStamps.init ())
val START_TIME = ref (!base)
val STOP_TIME = ref (TimeStamps.add (!START_TIME))
val now = ref (TimeStamps.add (!START_TIME))
val frameStart = ref (!now)
val finger = ref (!now)

fun dPrint str = 
    if DEBUG then
	print str
    else
	()

fun dPrint str = ()

fun insertTime () = 
  let val t = TimeStamps.add (!now) 
  in 
    now := t; t
  end 

fun addTime () = TimeStamps.add (!now) 

fun advanceTime () = 
  case (TimeStamps.getNext (!now)) of
      NONE => raise InternalError
    | SOME t => now := t

fun setTime t = now := t

fun isOutOfFrame(start,stop) =
    not (TimeStamps.compare(!now,start) = LESS andalso 
        TimeStamps.compare(stop,!finger) = LESS)

(**********************************************************************
 ** Memo Table
 **********************************************************************)

(*structure MemoTable = BuildMemoTable (val isOutOfFrame = isOutOfFrame)*)

(**********************************************************************
 ** Statistics
 **********************************************************************)
val N_NO_READS = ref 0

fun inc r = r := (!r) + 1
fun reset r = r := 0
  

(**********************************************************************
 ** Types
 **********************************************************************)
structure Reader = 
  struct
    type t = (time * time * (unit -> unit)) 
    fun compare (a as (sa,_,_), b as (sb,_,_)) = (TimeStamps.compare(sa,sb))
    fun isValid (c,s,e) = not (TimeStamps.isSplicedOut s)
    fun toString (t1,t2,_) = "reader = " ^ (TimeStamps.toString t1)
  end
type reader = Reader.t

type 'a version = 'a * time * (reader list ref)
val emptyWrites = nil


val modrefID = ref ~1
fun nextMID () = 
    let val id = !modrefID 
	val _ = modrefID := id + 1 
    in
	id
    end

type 'a modref = (int * ('a * 'a -> bool) * (('a version list))) ref
type 'a t = 'a modref

(**********************************************************************
 ** Priority Queue
 **********************************************************************)
structure PQueue = LeftistHeap (structure Element=Reader)

type pq = PQueue.t
val PQ  = ref PQueue.empty
fun initQ() = (PQ := PQueue.empty)
fun printQ () = dPrint ("Queue = \n")
	      
fun insertQ (e as (t1, _, _)) =  
let
    val _ = dPrint ("--> Queing read start = " ^ TimeStamps.toString t1 ^ "\n")
in
    PQ := PQueue.insert (e,!PQ) 
end

fun deleteMinQ () = 
    let val (m,q) = PQueue.deleteMin (!PQ) 
      val _ = PQ := q
    in m end

fun resetQ () = PQ := PQueue.empty
fun checkQ () = dPrint ("queue size = " ^ Int.toString (PQueue.size  (!PQ)) ^ "\n")

val lastRead: reader option ref  = ref NONE
fun findMinQ () = 
  let 
    val (m,q) = PQueue.findMin (!PQ) 
    val _ = PQ := q
  in
    case m  of
      NONE => NONE
    | SOME (r as (_,t,_)) => 
	  if (TimeStamps.isSplicedOut t) then
	      (deleteMinQ ();  findMinQ ())
	  else
	    case (!lastRead) of
	      NONE => (lastRead := m; m)
	    | SOME (_,t',_) => 
	      if (TimeStamps.compare (t,t') = EQUAL) then
		(deleteMinQ (); findMinQ ())
	      else
		(lastRead :=  m; m)
  end

fun init () = 
    let
	val _ = PROPAGATION_IN_PROGRESS := false
	val _ = base := (TimeStamps.init ())
	val _ = START_TIME := (!base)
        val _ = STOP_TIME = ref (TimeStamps.add (!START_TIME))
	val _ = now := (TimeStamps.add (!START_TIME))
	val _ = frameStart := !now
	val _ = finger := !now
	val _ = reset N_NO_READS
	val _ =  initQ()
    in
	()
    end

(**********************************************************************
 ** Writes
 **********************************************************************)
(* Find the write that comes immediately before or at t0 *)
fun findWrite (ws,t0) = 
    let
	fun find (l,prev) =
	    case l of
		nil => (prev)
	      | (h as (v,t,rs))::rest => 
		case TimeStamps.compare (t,t0) of
		    LESS => (find (rest, SOME h))
		  | GREATER => prev
		  | EQUAL => SOME h
    in
	find (ws,NONE)
    end

fun printOption r =
  case r of
      NONE => dPrint "option: none"
    | SOME x => dPrint "option: some"


fun findLastWrite ws = 
    let
	fun find (l,prev) =
	    case l of
		nil => (prev)
	      | (h as (v,t,rs))::rest => (find (rest, SOME h))
	val r = find (ws,NONE)
	val _ = printOption r
    in
	r
    end


fun queueReaders (rdrs) = List.app (fn r => (dPrint "!"; insertQ r)) rdrs

fun moveReaders (rdrs, dest) = 
    dest := List.@ (!dest, List.filter (fn (t,_,_) => not (TimeStamps.isSplicedOut t)) rdrs)


fun splitReaders eq (w_prev as (v_prev, t_prev, r_prev), w_next as (v_next,t_next,r_next)) = 
    let
        (** Find the readers that happen after t_next**)
	fun split l =  
	    case l of 
		nil => (nil,nil)
	      | (r as (t,_,_))::rest => 
		case TimeStamps.compare (t,t_next) of
		    LESS => let val (a,b) = split rest in (r::a, b) end
		  | GREATER => let val (a,b) = split rest in (a, r::b) end
		  | _ => raise InternalError

        (** Find those readers of r_prev that will be affected **)
	val (less,greater) = split (!r_prev)
	(** Keep unaffected ones in r_prev **)
	val _ = r_prev := less
	val _ = dPrint "splitting readers\n" 
    in
	(** Append, or rerun the affected ones **)
        if eq (v_prev,v_next) then  
	    (dPrint "equal. appending"; 
	     r_next := List.@ (!r_next, greater))
	else
	    (dPrint "non-equal.  queueing";
	     (case greater of
		  nil => dPrint "empty though\n"
		| _  => dPrint "non-empty, him\n");
	     queueReaders greater)
    end

fun splitReaders eq (w_prev as (v_prev, t_prev, r_prev), w_next as (v_next,t_next,r_next)) = 
    let
        (** Find the readers that happen after t_next**)
	fun split l =  
	    case l of 
		nil => (nil,nil)
	      | (r as (t,_,_))::rest => 
		case TimeStamps.compare (t,t_next) of
		    LESS => let val (a,b) = split rest in (r::a, b) end
		  | GREATER => let val (a,b) = split rest in (a, r::b) end
		  | _ => raise InternalError

        (** Find those readers of r_prev that will be affected **)
	val (less,greater) = split (!r_prev)

	(** Keep unaffected ones in r_prev **)
	val _ = r_prev := less
	val _ = dPrint "splitting readers\n" 
	val _ = r_next := List.@ (!r_next, greater)
    in
	(** Append, or rerun the affected ones **)
        if eq (v_prev,v_next) then  
	    (dPrint "equal. skipping\n")
	else
	    (dPrint "non-equal.  queueing\n";
	     (case greater of
		  nil => dPrint "empty though\n"
		| _  => dPrint "non-empty, him\n");
	     queueReaders greater)
    end

fun insertWrite  eq (w as (_,tw,_), ws) = 
    let
	(* Insert the write into the write list in sorted increasing
         * order and activate the readers of the previous write (if any) that
         * come after the write
         *)
	fun insert (l,prev) =  
	    case l of
		nil => 
		let
		    val _ = case prev of
				NONE => ()
			      | SOME w_prev => splitReaders eq (w_prev, w)

		in 
		    w::nil
		end
	      | (w' as (v',t',_))::rest => 
		case TimeStamps.compare (t',tw) of
		    LESS => w'::(insert (rest,SOME w'))
		  | GREATER => 
		    let val _ = case prev of
				    NONE => ()
				  | SOME w_prev => splitReaders eq (w_prev,w)
		    in w::w'::rest
		    end
		  | EQUAL => raise Impossible
    in
	insert (ws,NONE)
    end


fun deleteWrite  (m: 'a modref, tw) () = 
    let
	val (id,eq,ws) = !m

	fun delete (l,prev) =  
	    case l of
		nil => raise InternalError  (* must have found the write w *)
	      | (w as (v,t,rdrs))::rest => 
		case TimeStamps.compare (t,tw) of
		    LESS => w::(delete (rest,SOME w))
		  | GREATER => raise InternalError (* must have found the write w *)
		  | EQUAL => 
		    let 
			val _ = 
			    case prev of
				NONE => ((*raise TROUBLE; *) queueReaders (!rdrs))
			      | SOME (v_prev,_,r_prev) => 
				let
				    val _ = moveReaders (!rdrs, r_prev)
                                in
				    if (eq (v_prev,v)) then
					(dPrint "eq";(* moveReaders (!rdrs, r_prev) *) ())
				    else
					(queueReaders (!rdrs))
				end
		    in
			rest
		    end
	val ws' = delete (ws,NONE)
    in
	m := (id,eq,ws')
    end

fun changeInitialWrite (m: 'a modref, v) = 
    let
	val (id,eq,ws) = !m
    in
	case ws of
	    nil =>  (** If there are no writes, then insert a write **)
	    let
		val w = (v,!START_TIME,ref nil)
		val ws' = insertWrite eq (w,ws)
	    in
		m :=  (id,eq,ws')
	    end

	  | (h as (v',t,r: reader list ref))::rest => 
	    if eq (v,v') then
		(dPrint "no change!!\n")
	    else
		let 
		    val w = (v, t, r)  (* Note that we are preserving the time stamp *) 
		    val _ =  queueReaders (!r)
		    val ws' = w::rest
		in
		    m := (id,eq,ws')
		end
    end

fun insertRead (r, (w as (_,_,rrs))) = rrs := r::(!rrs)

(**********************************************************************
 ** Modifiables
 **********************************************************************)
fun allocMod eq = ref (nextMID (), eq, nil)
fun allocEqMod () = allocMod (fn (a,b) => a=b)



fun keyOfMod m = 
    let val (id,_,_) = !m 
    in 
	id
    end

fun write (m: 'a modref) v =
    let
        val (id,eq, ws) = !m
	val t = insertTime ()
	val _ = dPrint ("Writing mod ID = " ^ Int.toString id ^ 
		       "at time = " ^ TimeStamps.toString t ^ "\n") 
	val w = (v,t,ref nil)
	val _ = dPrint ("Before write, write list len = " ^ Int.toString (List.length ws) ^ "\n")
	val ws' =  insertWrite eq (w,ws)
	val _ = dPrint ("After write, write list len = " ^ Int.toString (List.length ws') ^ "\n")
	val _ = TimeStamps.setInv (t, deleteWrite (m,t))
    in
	m := (id,eq,ws')
    end

fun modFromVal eq v = 
    let
	val m = allocMod eq
	val _ = write  m v
    in
	m
    end
  
fun read m f = 
    let

	val t1 = insertTime ()
	val t2 = addTime ()

	fun run () = 
	    let
		val (id,eq,ws) = !m
	    in
		case (findWrite (ws,t1)) of
		    NONE => raise UnsetMod
		  | SOME (w as (v,tw,rrs)) => 
		    let
			val _ = dPrint "Read rerun\n"
			val _ = f v
			val _ = insertRead ((t1,t2,run),w)
		    in
			()
		    end
	    end

	val () = run ()
	val _  = advanceTime ()
    in
	()
    end


fun read m f = 
    let
	val wr = ref NONE
	fun run t1 () = 
	    let
		val (id,eq,ws) = !m
	    in
		case (findWrite (ws,t1)) of
		    NONE => raise UnsetMod
		  | SOME (w as (v,tw,rrs)) => 
		    if (TimeStamps.isSplicedOut tw) then
			raise InternalError
		    else
			let
			    val _ = f v
			    val _ =  wr :=  SOME w
			in
			    ()
			end
	    end

	val t1 = insertTime ()
	val _ = dPrint ("++ Read = start = " ^ TimeStamps.toString t1 ^ "\n")

	val () = run t1 ()
	val t2  = insertTime ()
	val _ = case (!wr) of
		    NONE => ()
		  | SOME w => insertRead ((t1,t2,run t1),w)
    in
	()
    end

fun modToString toString m = 
  let
      val (id,eq,ws) = !m
      fun loop ws = 
	  case ws of
	      nil => ""
	    | (v,_,_)::rest => toString v ^ " " ^ (loop rest)
  in
      loop ws
  end


(**********************************************************************
 ** Meta operations
 **********************************************************************)
(*  This cannot be, we want to deref at the current time.
fun deref m = 
    let
	val (id,eq, ws) = !m
    in
	case ws of
	    nil => raise UnsetMod
	  | (v,_,_)::_ => v
    end
*)


fun deref m = 
    let
	val (id,eq,ws) = !m
	val _ = dPrint ("Dereffing id = " ^ Int.toString id ^ "\n")
	val _ = dPrint ("write list len = " ^ Int.toString (List.length ws) ^ "\n")
    in
	if (!PROPAGATION_IN_PROGRESS) then
	    case (findWrite (ws,!now)) of
		NONE => raise UnsetMod
	      | SOME (v,tw,_) => 
		if (TimeStamps.isSplicedOut tw) then
		    raise InternalError
		else
		    v
	else
	    case (findLastWrite ws) of
		NONE => (dPrint "unset" ;raise UnsetMod )
	      | SOME (v,tw,_) => 
		if (TimeStamps.isSplicedOut tw) then
		    raise InternalError
		else
		    (dPrint "Set"; v)
    end

fun derefInitial m = 
    let
	val  _ = dPrint ".derefInitial"
	val (id,eq, ws) = !m
    in
	case ws of
	    nil => raise UnsetMod
	  | (v,_,_)::_ => v
    end

fun change m v =  changeInitialWrite (m,v)
  
fun numTimeStamps () = TimeStamps.size (!now)


fun printTimeStamps (sa,a,sb,b) = 
    dPrint (sa ^ " = " ^ TimeStamps.toString a ^
	   sb ^ " = " ^ TimeStamps.toString b ^ "\n")


fun propagateUntil (endTime) =  
  let
    fun loop  () = 
      case  (findMinQ ()) of
	NONE => ()
      | SOME(start,stop,f) =>
	let
	    val _ = dPrint "$$ CP: read: " 
(*	    val _ = printQ () *)

	    val _ = printTimeStamps ("start", start, "stop", stop)
	in
	    if (TimeStamps.compare (endTime,stop)=LESS) then  ()
	    else if (TimeStamps.compare (start, !now)= LESS) then 
		(printTimeStamps ("$CP: start", start, "now", !now);	     
		 raise InternalError)
	    else
	    let
		val _ = deleteMinQ ()
		val finger' = (!finger)
		val _ = now := start;	   
		val _ = finger := stop;
		val _ = f();
		val _ = finger := finger';
		val _ = TimeStamps.spliceOut (!now,stop) handle e => raise e; 
            in
		loop ()	
	    end
	end
  in	    
    (loop (); now := endTime)
  end

fun propagate () =  
  let
      val _ = PROPAGATION_IN_PROGRESS := true
    val _ = dPrint ("Time Stamps = \n")
    val _ = now := (!START_TIME)  
    fun loop  () = 
      case  (findMinQ ()) of
	NONE => 0
      | SOME(start,stop,f) =>
	if (TimeStamps.compare (start,!now)= LESS) then 
		(printTimeStamps ("$$ CP.Main = start", start, "now", !now);	     
                 dPrint ("Time Stamps =\n");
		 printQ ();
		 raise InternalError)
	else 
	  let
	      val _ = dPrint "$$ CP.Main : read: " 
	      val _ = printQ () 

	      val _ = printTimeStamps ("start", start, "stop", stop)
	      val _ = dPrint "rerunning\n"
              val _ = deleteMinQ ()
              val _ = now := start
	      val _ = finger := stop
              val _ = f()
	      val _ = TimeStamps.spliceOut (!now,stop) handle e => raise e
	      val _ = dPrint "done\n"
          in
              1+loop ()	
	  end
       val r = loop ()
       val _ = dPrint ("Time Stamps = \n");
       val _ = dPrint "*** CHANGE PROPATION COMPLETE ***\n"

       val _ = PROPAGATION_IN_PROGRESS := false
    in	    
	r
    end



(**********************************************************************
 ** Memoization
 **********************************************************************)
(*
fun mkMemoPure () =  
  let
      fun memoPure table f key =
	  let
	      fun run_memoized (mref,t) = 
		  let
		      val v = f()
		      val _ =  mref := SOME(v,[t]) 
		  in v end

	      val t = insertTime ()
	      val mref = PureMemoTable.find (table, key, t)
	  in
	      case !mref of
		  NONE => run_memoized (mref,t)
		| SOME(v,_) => v
	  end

      val table = PureMemoTable.new ()
      fun memo arg f = memoPure table f arg
  in
      memo
  end


fun mkMemo () = 
    let
	val table = MemoTable.new ()

	(** Insert the entry into the memo table at r **)
	fun insert delete (mref,entry,(t1,t2,nt1o)) = 
	    let
		val (start,stop) = 
		    case nt1o of 
			NONE =>  
			let val t3 = insertTime ()
			in (t3,t3)
			end
		      | SOME(nt1) =>
			if (TimeStamps.compare (nt1,t2)=LESS) then (nt1,t2)
			else let val t3 = insertTime ()
			     in (t3,t3)
			     end
		val _ = TimeStamps.addToInv (stop, fn () => delete (start,stop)) 
	    in
		mref := SOME(entry, start,stop)
	    end

	fun spliceIn (t1,t2) = 
	    let
		val _ = dPrint "MEMO: re-using: " 
		val _ = printTimeStamps ("start", t1, "stop", t2)
		val _ = TimeStamps.spliceOut (!now,t1)
		val _ = now := t1
	    in
		()
	    end
	    
	fun update (g, t2) = (g (); propagateUntil t2)

	fun mfun key f =
	    let
		fun run_memoized (f,mref) = 
		    let
			val t1 = !now
			val v = f()
			val t2 = !now
			val nt1o = TimeStamps.getNext t1
			val _ = insert  (MemoTable.delete (table,key)) (mref,v,(t1,t2,nt1o))
		    in
			v
		    end
 
		val mref = MemoTable.find (table, key, !now)
	    in
		case !mref of
		    NONE => run_memoized (f, mref)
		  | SOME(v,t1,t2) =>(spliceIn (t1,t2);
				     update (fn () => (), t2);
				     v) 
	    end
    in
	mfun
    end
*)

end 
