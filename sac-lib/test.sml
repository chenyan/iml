structure C = Comb
structure M = Meta

type 'a modref = 'a Modref.modref
datatype 'a list = Nil | Cons of ('a * 'a list modref)

val write = C.write'
val modref = C.modref
val read = C.read

val tuple : (int * int) = (1 , 2)
and l = modref ((write MLton.eq (Cons (1 , (modref (write MLton.eq (Cons (2 , (modref (write MLton.eq (Cons (3 : int , (modref (write MLton.eq Nil))))))))))))))
val add = op+
val rec inc = fn x_18747 => (fn (x) => (add (x , 1))) ((x_18747))
val rec map = fn x_18748 =>
fn x_18749 =>
  (fn (f , l) =>
   read ((l) , (fn Nil => (write MLton.eq Nil)
                | (Cons (h , t)) =>
                  (write MLton.eq (Cons ((f h) , (modref ((map f) t)))))))) ((x_18748 , x_18749))
val l = modref (((map inc) l))

val x_1 = modref (((map inc) l))
val _ = modref(read (x_1, fn Cons(h,t) => write (print ((Int.toString h)))))
(*
val Nil = modref (((map inc) l))
*)



