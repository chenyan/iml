(**********************************************************************
 ** MEMO_TABLE.sml
 **
 ** Copyright 2005 Umut A. Acar
 **
 ** Written by Umut A. Acar and Guy Blelloch
 **
 ** This program is distributed under the terms of the GNU General
 ** Public License as published by the Free Software Foundation.  See
 ** the LICENSE file for the terms and conditions.
 **
 ***************
 ** Description: The signature for memo tables.
 **********************************************************************)

signature MEMO_TABLE = 
sig
  type 'a memotable
  type 'a t = 'a memotable

  type 'a memotable_one
  type 'a t_one = 'a memotable_one

  type 'a entry = ('a * TimeStamps.t * TimeStamps.t) option ref

  val new: unit -> 'a t
  val numItems: 'a t -> int
  val find: 'a t * int list * TimeStamps.t -> 'a entry
  val delete: ('a t * int list) ->  (TimeStamps.t * TimeStamps.t) -> unit


  val newOne: unit -> 'a t_one
  val numItemsOne: 'a t_one -> int
  val findOne: 'a t_one * int list * TimeStamps.t -> 'a entry
  val deleteOne: ('a t_one * int list) -> (TimeStamps.t * TimeStamps.t) -> unit
end
