
(*******************************************************************************)
(* MLton Profiling Code                                                        *)
(* Reproduced from the cps codebase.                                           *)
(*******************************************************************************)
structure Stats =
struct
type u = {usr:Time.time,sys:Time.time,tot:Time.time}
type t = {nongc:u,gc:u,total:u}
	 
val zero =
    let
        val zeroU = {usr = Time.zeroTime, 
                     sys = Time.zeroTime, 
                     tot = Time.zeroTime}
    in
        {nongc = zeroU, gc = zeroU, total = zeroU}
    end
    
fun wrap (f : 'a -> 'b, arg: 'a) : ('b * t) =
    let
        val cpu_timer = Timer.startCPUTimer ()
        val res = f arg
        val {nongc,gc} = Timer.checkCPUTimes cpu_timer
        val nongc_usr = #usr nongc
        val nongc_sys = #sys nongc
        val nongc_tot = Time.+ (nongc_usr, nongc_sys)
        val gc_usr = #usr gc
        val gc_sys = #sys gc
        val gc_tot = Time.+ (gc_usr, gc_sys)
        val total_usr = Time.+ (nongc_usr, gc_usr)
        val total_sys = Time.+ (nongc_sys, gc_sys)
        val total_tot = Time.+ (total_usr, total_sys)
    in
        (res,
         {nongc = {usr = nongc_usr, sys = nongc_sys, tot = nongc_tot},
          gc = {usr = gc_usr, sys = gc_sys, tot = gc_tot},
          total = {usr = total_usr, sys = total_sys, tot = total_tot}})
    end
fun add (s1: t, s2: t) = 
    let
        fun addU (s1: u, s2: u) =
            {usr = Time.+ (#usr s1, #usr s2),
             sys = Time.+ (#sys s1, #sys s2),
             tot = Time.+ (#tot s1, #tot s2)}
    in
        {nongc = addU (#nongc s1, #nongc s2),
         gc = addU (#gc s1, #gc s2),
         total = addU (#total s1, #total s2)}
    end
    
fun toString ({nongc, gc, total} : t) =
    concat ["nongc: ",
            "usr: ", (Time.toString (#usr nongc)), "  ",
            "sys: ", (Time.toString (#sys nongc)), "  ",
            "total: ", (Time.toString (#tot nongc)), "\n",
            "gc: ", 
            "usr: ", (Time.toString (#usr gc)), "  ",
            "sys: ", (Time.toString (#sys gc)), "  ",
            "total: ", (Time.toString (#tot gc)), "\n",
            "total: ", 
            "usr: ", (Time.toString (#usr total)), "  ",
            "sys: ", (Time.toString (#sys total)), "  ",
            "total: ", (Time.toString (#tot total))]

fun toString ({nongc,gc,total}:t) =
    concat [Time.toString (#tot nongc), " ",
            Time.toString (#tot gc), " ",
            Time.toString (#tot total)]
end
(********************************************************************************)

fun runexp out round (experiment,arg) =
let
  val (res, stats) = Stats.wrap (experiment,arg)
  val time = Int.toString round ^ " " ^ Stats.toString stats ^ "\n";
  val _ = TextIO.output (out, time)
  val _ = print time
in
  (res,stats)
end
