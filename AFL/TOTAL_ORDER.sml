(******************************************************************************
 ** TOTAL_ORDER.sml
 **
 ** Copyright 2005 Umut A. Acar
 **
 ** Written by Umut A. Acar
 **
 ** This program is distributed under the terms of the GNU General
 ** Public License as published by the Free Software Foundation.  See
 ** the LICENSE file for the terms and conditions.
 **
 ***************
 ** Description: Interface for a total ordering.
 **********************************************************************)
signature TOTAL_ORDER =
sig
  type t
  val compare : t * t -> order
  val isValid : t -> bool
end
